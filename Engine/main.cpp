#include "WECore.h"
#include "Math/MathLibrary.h"
#include "PIL/Application/Application.h"
#include "Scene/Scene.h"

using namespace WindmillEngine;

class MyApplication : public Application
{
public:
	// TODO: Does it make sense for the application hold the Scene?
	MyApplication(SString&& InTitle)
		: Application(InTitle)
	{
		WScene* Scene = new WScene();
		Scene->Initialize();

		EEntity* En0 = new EEntity();
		WScene::GetScene()->SpawnEntity(En0, Math::SVector3D(0, 1, 2), Math::SVector3D(45, 0, 0));

		ECameraEntity* Cam = new ECameraEntity();
		WScene::GetScene()->SpawnEntity(Cam);
	}

	~MyApplication()
	{
		WE_WARN("Destroying Application");
		WScene* Scene = WScene::GetScene();
		delete Scene;
	}

	void OnStart() override
	{
		WScene::GetScene()->Start();
	}

	void OnUpdate(const Time& InTime) override
	{
		WScene::GetScene()->Update();
		SetApplicationState(EApplicationState::AS_Exiting);
	}

	void OnExit() override
	{
		WScene::GetScene()->Exit();
		WE_INFO("Exitting");
	}
};

int main(int argc, char* argv[])
{
	{
		MyApplication App(TEXT("My App"));

		App.Initialize();
		App.Start();
	}

	return 0;
}