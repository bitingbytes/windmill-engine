#pragma once

#include "WECore.h"

namespace WindmillEngine
{
	class WClass
	{
	public:
		WClass(const std::string& InName, const WClass* InBaseType);

		~WClass();

		bool operator==(const WClass &Other) const;
		bool operator!=(const WClass &Other) const;

		forceinline std::string GetName() const
		{
			return m_ClassName;
		}

	private:
		std::string m_ClassName;

		const WClass* m_BaseType;
	};

#define WCLASS_DEF() \
public: \
	static const WClass TYPE; \
	\
	virtual const WClass& GetType() const \
	{ \
		return TYPE; \
	}

#define WCLASS_DEC(Type, Name, Parent) \
	const WClass Type::TYPE(#Name, &Parent::TYPE)

} // WindmillEngine