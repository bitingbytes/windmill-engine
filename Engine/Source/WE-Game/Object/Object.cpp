#include "WECorePCH.h"
#include "Object.h"

namespace WindmillEngine
{
	const WClass WObject::TYPE("WObject", nullptr);

	const WClass& WObject::GetType() const
	{
		return TYPE;
	}

	uint32 WObject::sLastID = 0;

	WObject::WObject(const SString& InName)
		: m_Name(InName)
		, m_GUID()
		, m_ID()
	{
	}

	WObject::~WObject()
	{
	}

	// TODO: Fazer direito.
	void WObject::MakeUniqueName()
	{
#if _UNICODE
		SString Temp = TEXT(" ") + std::to_wstring(m_ID);
#else
		SString Temp = TEXT(" ") + std::to_string(m_ID);
#endif
		m_Name += Temp;
	}
} // WindmillEngine