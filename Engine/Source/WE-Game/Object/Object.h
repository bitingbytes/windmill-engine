#pragma once

#include "WECore.h"
#include "Class.h"
#include "PIL/Utils/GUID.h"

namespace WindmillEngine
{
	// TODO: Should I use virtual base class here?
	class WObject
	{
	public:
		virtual const WClass& GetType() const;
		static const WClass TYPE;

		virtual ~WObject();		

		forceinline SString GetName() const
		{
			return m_Name;
		}

		forceinline void SetName(const SString& NewName)
		{
			m_Name = NewName;
			MakeUniqueName();
		}

	protected:
		WObject(const SString& InName);

	private:
		SUID m_GUID;

		uint32 m_ID;
		static uint32 sLastID;

		SString m_Name;

		void MakeUniqueName();
	};
} // WindmillEngine