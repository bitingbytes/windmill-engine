#include "WECorePCH.h"
#include "Class.h"

namespace WindmillEngine
{
	WClass::WClass(const std::string& InName, const WClass * InBaseType)
		: m_ClassName(InName)
		, m_BaseType(InBaseType)
	{
	}

	WClass::~WClass()
	{
	}

	bool WClass::operator==(const WClass& Other) const
	{
		return m_ClassName.compare(Other.m_ClassName) == 0 && m_BaseType == Other.m_BaseType;
	}

	bool WClass::operator!=(const WClass& Other) const
	{
		return m_ClassName.compare(Other.m_ClassName) != 0;
	}
} // WindmillEngine