#include "CameraComponent.h"

namespace WindmillEngine
{
	WCLASS_DEC(WCameraComponent, CameraComponent, WTransformComponent);

	WCameraComponent::WCameraComponent()
		: m_FieldOfView(90.0f)
		, m_NearPlane(0.01f)
		, m_FarPlane(5000.0f)
		, m_ProjectionType(EProjectionType::PT_Perspective)
		, m_ClearColor(Math::SColor32(0.25f, 0.5f, 0.75f))
		, m_Viewport(Math::SViewport())
		, m_ViewMatrix(1.0f)
		, m_ProjectionMatrix(1.0f)
		, m_Renderer(nullptr)
	{
		memset(m_FrustumPlanes, 0, sizeof(m_FrustumPlanes));
	}

	WCameraComponent::~WCameraComponent()
	{
	}

	void WCameraComponent::Update()
	{
		m_ViewMatrix = Math::SMatrix4x4::Translate(-GetWorldPosition()) * GetRotationMatrix();

		float Ratio = m_Viewport.Width / m_Viewport.Height;

		if (m_ProjectionType == EProjectionType::PT_Perspective)
			m_ProjectionMatrix = Math::SMatrix4x4::Perspective(m_NearPlane, m_FarPlane, Ratio, m_FieldOfView);
		else
			m_ProjectionMatrix = Math::SMatrix4x4::Orthogonal();

		ConstructFrustum(m_ViewMatrix, m_ProjectionMatrix);
	}

	void WCameraComponent::ConstructFrustum(const Math::SMatrix4x4& InViewMatrix, const Math::SMatrix4x4& InProjMatrix)
	{
		Math::SMatrix4x4 Matrix = InViewMatrix * InProjMatrix;
		Matrix = Matrix.Transposed();

		m_FrustumPlanes[0].X = Matrix.E[0][3] + Matrix.E[0][2];
		m_FrustumPlanes[0].Y = Matrix.E[1][3] + Matrix.E[1][2];
		m_FrustumPlanes[0].Z = Matrix.E[2][3] + Matrix.E[2][2];
		m_FrustumPlanes[0].W = Matrix.E[3][3] + Matrix.E[3][2];
		m_FrustumPlanes[0] = m_FrustumPlanes[0].Normalized();

		m_FrustumPlanes[1].X = Matrix.E[0][3] - Matrix.E[0][2];
		m_FrustumPlanes[1].Y = Matrix.E[1][3] - Matrix.E[1][2];
		m_FrustumPlanes[1].Z = Matrix.E[2][3] - Matrix.E[2][2];
		m_FrustumPlanes[1].W = Matrix.E[3][3] - Matrix.E[3][2];
		m_FrustumPlanes[1] = m_FrustumPlanes[1].Normalized();

		m_FrustumPlanes[2].X = Matrix.E[0][3] + Matrix.E[0][0];
		m_FrustumPlanes[2].Y = Matrix.E[1][3] + Matrix.E[1][0];
		m_FrustumPlanes[2].Z = Matrix.E[2][3] + Matrix.E[2][0];
		m_FrustumPlanes[2].W = Matrix.E[3][3] + Matrix.E[3][0];
		m_FrustumPlanes[2] = m_FrustumPlanes[2].Normalized();

		m_FrustumPlanes[3].X = Matrix.E[0][3] - Matrix.E[0][0];
		m_FrustumPlanes[3].Y = Matrix.E[1][3] - Matrix.E[1][0];
		m_FrustumPlanes[3].Z = Matrix.E[2][3] - Matrix.E[2][0];
		m_FrustumPlanes[3].W = Matrix.E[3][3] - Matrix.E[3][0];
		m_FrustumPlanes[3] = m_FrustumPlanes[3].Normalized();

		m_FrustumPlanes[4].X = Matrix.E[0][3] - Matrix.E[0][1];
		m_FrustumPlanes[4].Y = Matrix.E[1][3] - Matrix.E[1][1];
		m_FrustumPlanes[4].Z = Matrix.E[2][3] - Matrix.E[2][1];
		m_FrustumPlanes[4].W = Matrix.E[3][3] - Matrix.E[3][1];
		m_FrustumPlanes[4] = m_FrustumPlanes[4].Normalized();

		m_FrustumPlanes[5].X = Matrix.E[0][3] + Matrix.E[0][1];
		m_FrustumPlanes[5].Y = Matrix.E[1][3] + Matrix.E[1][1];
		m_FrustumPlanes[5].Z = Matrix.E[2][3] + Matrix.E[2][1];
		m_FrustumPlanes[5].W = Matrix.E[3][3] + Matrix.E[3][1];
		m_FrustumPlanes[5] = m_FrustumPlanes[5].Normalized();
	}

	bool WCameraComponent::InsideFrustum(Math::SBBox const & box)
	{
		// check box outside/inside of frustum
		for (int i = 0; i < 6; i++)
		{
			int out = 0;
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Min.X, box.Min.Y, box.Min.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Max.X, box.Min.Y, box.Min.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Min.X, box.Max.Y, box.Min.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Max.X, box.Max.Y, box.Min.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Min.X, box.Min.Y, box.Max.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Max.X, box.Min.Y, box.Max.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Min.X, box.Max.Y, box.Max.Z, 1.0f)) < 0.0) ? 1 : 0);
			out += ((Math::SVector4D::Dot(m_FrustumPlanes[i], Math::SVector4D(box.Max.X, box.Max.Y, box.Max.Z, 1.0f)) < 0.0) ? 1 : 0);
			if (out == 8)
				return false;
		}

		// check frustum outside/inside box
		/*
		int out;
		out = 0; for (int i = 0; i<8; i++) out += ((fru.mPoints[i].x > box.mMaxX) ? 1 : 0); if (out == 8) return false;
		out = 0; for (int i = 0; i<8; i++) out += ((fru.mPoints[i].x < box.mMinX) ? 1 : 0); if (out == 8) return false;
		out = 0; for (int i = 0; i<8; i++) out += ((fru.mPoints[i].y > box.mMaxY) ? 1 : 0); if (out == 8) return false;
		out = 0; for (int i = 0; i<8; i++) out += ((fru.mPoints[i].y < box.mMinY) ? 1 : 0); if (out == 8) return false;
		out = 0; for (int i = 0; i<8; i++) out += ((fru.mPoints[i].z > box.mMaxZ) ? 1 : 0); if (out == 8) return false;
		out = 0; for (int i = 0; i<8; i++) out += ((fru.mPoints[i].z < box.mMinZ) ? 1 : 0); if (out == 8) return false;
		*/
		return true;
	}
} // WindmillEngine