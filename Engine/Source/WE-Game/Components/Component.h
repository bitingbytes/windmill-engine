#pragma once

#include "Object/Object.h"

namespace WindmillEngine
{
	class WComponent : public WObject
	{
		WCLASS_DEF()
	public:
		virtual ~WComponent();

		/// <summary>
		/// Called after the Component is added to an Entity.
		/// </summary>
		virtual void OnAttach();

		/// <summary>
		/// Called after the Component is detached from an Entity.
		/// </summary>
		virtual void OnDetach();

		/// <summary>
		/// Called right after the Owner(Entity) is Initialized.
		/// </summary>
		virtual void InitializeComponent();

		/// <summary>
		/// Called when InitializeComponent is done.
		/// </summary>
		virtual void OnInitialize();

		virtual void Start();

		virtual void Update();

		/// <summary>
		/// Tells the Garbage Collector that this component wants to die.
		/// </summary>
		virtual void Exit();

		void SetOwner(class EEntity* NewOwner);

		forceinline class EEntity* GetOwner() const
		{
			return m_Owner;
		}

		forceinline bool IsExclusive() const
		{
			return bIsExclusive;
		}
	protected:
		WComponent(bool IsExclusive = false) noexcept;

		class EEntity* m_Owner;

	private:
		uint8 bIsExclusive : 1;
		uint8 bIsInitialized : 1;
	};

	typedef std::shared_ptr<WComponent> WComponentPtr;
} // WindmillEngine