#include "WECorePCH.h"
#include "TransformComponent.h"
#include "Entity/Entity.h"

namespace WindmillEngine
{
	WCLASS_DEC(WTransformComponent, TransformComponent, WComponent);

	WTransformComponent::WTransformComponent() noexcept
		: WComponent(true)
		, WSceneNode(TEXT("Transform Component"))
		, m_Position(Math::SVector3D::Zero)
		, m_Rotation(Math::SVector3D::Zero)
		, m_Scale(Math::SVector3D::One)
		, m_Mobility(EMobility::M_STATIC)
		, m_Up(Math::SVector3D::Up)
		, m_Forward(Math::SVector3D::Forward)
		, m_Right(Math::SVector3D::Right)
	{
	}

	WTransformComponent::~WTransformComponent()
	{
	}

	void WTransformComponent::Start()
	{
		CalculateWorldMatrix();
	}

	void WTransformComponent::Update()
	{
		WComponent::Update();

		if (m_Mobility == EMobility::M_DYNAMIC)
		{
			CalculateWorldMatrix();
		}
	}

	void WTransformComponent::CalculateWorldMatrix()
	{
		if (GetParent())
		{
			m_ObjectToWorld = GetParent()->GetObjectToWorldMatrix() * m_ObjectMatrix;
		}
		else
		{
			m_ObjectToWorld = m_ObjectMatrix;
		}

		m_Position = m_ObjectToWorld.Row[3].XYZ();

		m_Up = (m_Up * Math::SMatrix4x4::Rotate(m_Rotation)).XYZ();
		m_Forward = (m_Forward * Math::SMatrix4x4::Rotate(m_Rotation)).XYZ();
		m_Right = (m_Right * Math::SMatrix4x4::Rotate(m_Rotation)).XYZ();
	}
} // WindmillEngine