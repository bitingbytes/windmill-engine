#pragma once

#include "TransformComponent.h"

namespace WindmillEngine
{
	enum class EProjectionType
	{
		PT_Perspective,
		PT_Orthogonal
	};

	class WCameraComponent : public WTransformComponent
	{
		WCLASS_DEF()
	public:
		WCameraComponent();
		~WCameraComponent();

		virtual void Update() override;

		void ConstructFrustum(const Math::SMatrix4x4& InViewMatrix, const Math::SMatrix4x4& InProjMatrix);

		bool InsideFrustum(Math::SBBox const & box);
	private:
		float m_FieldOfView;
		float m_NearPlane;
		float m_FarPlane;
		EProjectionType m_ProjectionType;
		Math::SColor32 m_ClearColor;
		Math::SViewport m_Viewport; //TODO: Implement SViewport

		Math::SMatrix4x4 m_ViewMatrix;
		Math::SMatrix4x4 m_ProjectionMatrix;
		Math::SVector4D m_FrustumPlanes[6];

		//TODO: Implement Renderer
		class Renderer* m_Renderer;
	};
} // WindmillEngine