#pragma once

#include "Component.h"
#include "Scene/SceneNode.h"

namespace WindmillEngine
{
	enum class EMobility
	{
		M_STATIC,
		M_DYNAMIC
	};

	class WTransformComponent : public WComponent, public WSceneNode
	{
		WCLASS_DEF()
	public:
		WTransformComponent() noexcept;
		~WTransformComponent();

		virtual void Start() override;
		virtual void Update() override;

		forceinline void SetWorldPosition(const Math::SVector3D& NewPosition)
		{
			m_ObjectMatrix *= Math::SMatrix4x4::Translate(NewPosition);
			CalculateWorldMatrix();
		}

		forceinline void SetWorldRotation(const Math::SVector3D& NewRotation)
		{
			m_Rotation = NewRotation;
			m_ObjectMatrix *= Math::SMatrix4x4::Rotate(m_Rotation);
			CalculateWorldMatrix();
		}

		forceinline void SetWorldScale(const Math::SVector3D& NewScale)
		{
			m_Scale = NewScale;
			m_ObjectMatrix *= Math::SMatrix4x4::Scale(m_Scale);
			CalculateWorldMatrix();
		}

		forceinline Math::SVector3D GetWorldPosition() const
		{
			return m_ObjectToWorld.Row[3].XYZ();
		}

		forceinline Math::SMatrix4x4 GetRotationMatrix() const
		{
			return Math::SMatrix4x4::Rotate(m_Rotation);
		}

		forceinline Math::SVector3D GetLocalPosition() const
		{
			if (GetParent())
				return GetWorldPosition() - GetParent()->GetObjectToWorldMatrix().Row[3].XYZ();
			else
				return GetWorldPosition();
		}

	private:
		Math::SVector3D m_Position;
		Math::SVector3D m_Rotation; // Euler angles
		Math::SVector3D m_Scale;

		Math::SVector3D m_Up;
		Math::SVector3D m_Forward;
		Math::SVector3D m_Right;

		EMobility m_Mobility;

		void CalculateWorldMatrix();
	};
} // WindmillEngine