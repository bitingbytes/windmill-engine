#include "WECorePCH.h"
#include "Component.h"
#include "Entity/Entity.h"

namespace WindmillEngine
{
	WCLASS_DEC(WComponent, Component, WObject);

	WComponent::WComponent(bool IsExclusive) noexcept
		: WObject(TEXT("Component"))
		, m_Owner(nullptr)
		, bIsExclusive(IsExclusive)
		, bIsInitialized(false)
	{
	}

	WComponent::~WComponent()
	{
		m_Owner = nullptr;
	}

	void WComponent::OnAttach()
	{

	}

	void WComponent::OnDetach()
	{
	}

	// TODO: Think about when this is called. What if we AddComponents after the Entity is spawned?
	void WComponent::InitializeComponent()
	{
		bIsInitialized = true;
	}

	void WComponent::OnInitialize()
	{
	}

	void WComponent::Start()
	{
	}

	void WComponent::Update()
	{
	}

	void WComponent::Exit()
	{
	}

	void WComponent::SetOwner(EEntity * NewOwner)
	{
		if (m_Owner == nullptr)
		{
			m_Owner = NewOwner;
			// NewOwner->AttachNode(this);
		}
		else
			WE_ERROR("Component %s already has an owner!!!", GetName().c_str());
	}

} // WindmillEngine