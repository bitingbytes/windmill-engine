#pragma once

#include "WECore.h"
#include "Object/Object.h"
#include "Math/MathLibrary.h"

namespace WindmillEngine
{
	class WSceneNode : public WObject
	{
	public:
		WSceneNode(const SString& NewName);

		virtual ~WSceneNode();

		void SetParent(WSceneNode* InParent);

		forceinline WSceneNode* GetParent() const
		{
			return m_Parent;
		}	

		// protected:
		void AttachToNode(WSceneNode* NewParent);

		void AttachNode(WSceneNode* InNode, bool bForced = false);

		void DetachNode(WSceneNode* OutNode);

		forceinline Math::SMatrix4x4 GetObjectMatrix() const
		{
			return m_ObjectMatrix;
		}

		forceinline Math::SMatrix4x4 GetObjectToWorldMatrix() const
		{
			return m_ObjectToWorld;
		}

	protected:
		Math::SMatrix4x4 m_ObjectMatrix;
		Math::SMatrix4x4 m_ObjectToWorld;

		WSceneNode* m_Parent;
		std::vector<WSceneNode*> m_Children;
	};
} // WindmillEngine