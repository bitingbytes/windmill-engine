#include "SceneNode.h"

namespace WindmillEngine
{

	WSceneNode::WSceneNode(const SString& NewName)
		: WObject(NewName)
		, m_Parent(nullptr)
		, m_ObjectMatrix(1.0f)
		, m_ObjectToWorld(1.0f)
	{
	}

	WSceneNode::~WSceneNode()
	{
		if (m_Parent != nullptr)
		{
			SetParent(nullptr);
			for (WSceneNode* Node : m_Children)
			{
				delete Node;
			}
			m_Children.clear();
		}
	}

	void WSceneNode::SetParent(WSceneNode* InParent)
	{
		m_Parent = InParent;
	}

	void WSceneNode::AttachToNode(WSceneNode * NewParent)
	{
		NewParent->AttachNode(this);
	}

	void WSceneNode::AttachNode(WSceneNode* InNode, bool bForced)
	{
		WE_ASSERT(InNode);

		if (InNode->m_Parent)
		{
			if (bForced)
			{
				DetachNode(InNode);
			}
			else
			{
				WE_ERROR("Node %s already has a parent node!!!", InNode->GetName().c_str());
				return;
			}
		}

		InNode->SetParent(this);
		m_Children.push_back(InNode);
	}

	void WSceneNode::DetachNode(WSceneNode* OutNode)
	{
		WE_ASSERT(OutNode);
		m_Children.erase(std::find(m_Children.begin(), m_Children.end(), OutNode));
		m_Children.shrink_to_fit();
	}
} // WindmillEngine