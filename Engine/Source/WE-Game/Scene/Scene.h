#pragma once

#include "Cell.h"

#define MAX_CELLS_CHUNCK 4

// TODO: Scene MUST have an actived camera to be renderer.
namespace WindmillEngine
{
	/// <sumary>
	/// Scene is an array of cells.
	/// It is what the player sees.
	/// </sumary>
	class WScene : public WSceneNode
	{
	public:
		WScene() noexcept;
		~WScene();

		bool Initialize();

		void SpawnEntity(EEntity* InEntity, const Math::SVector3D InPosition = Math::SVector3D::Zero, const Math::SVector3D InRotation = Math::SVector3D::Zero, const Math::SVector3D InScale = Math::SVector3D::One);

		void Start();

		void Update();

		void Exit();

		// TODO: Is this copying all the Entities?
		forceinline std::vector<EEntity*> GetAllEntities() const
		{
			return m_Entities;
		}

		static WScene* GetScene();
	private:

		// TODO: Implement Cell Chunks.
		// WCell* m_ActivedCells[MAX_CELLS_CHUNCK];
		std::vector<EEntity*> m_Entities;

		ECameraEntity* m_ActivedCamera;

		static WScene* sCurrentScene;

		std::mutex m_Mutex;
	};
} // WindmillEngine