#include "WECorePCH.h"
#include "Scene.h"

namespace WindmillEngine
{
	WScene *WScene::sCurrentScene = nullptr;

	WScene::WScene() noexcept
		: WSceneNode(TEXT("Scene"))
	{
		m_Entities.reserve(32);
	}

	WScene::~WScene()
	{
		for (EEntity* Entity : m_Entities)
		{
			delete Entity;
		}
	}

	bool WScene::Initialize()
	{
		if (!sCurrentScene)
		{
			sCurrentScene = this;
			return true;
		}
		return false;
	}

	void WScene::SpawnEntity(EEntity *InEntity, const Math::SVector3D InPosition, const Math::SVector3D InRotation, const Math::SVector3D InScale)
	{
		AttachNode(InEntity->GetTransformComponent());

		InEntity->GetTransformComponent()->SetWorldPosition(InPosition);
		InEntity->GetTransformComponent()->SetWorldRotation(InRotation);
		InEntity->GetTransformComponent()->SetWorldScale(InScale);

		m_Entities.push_back(InEntity);

		// TODO: Do the same for lights
		if (InEntity->GetType().GetName() == "CameraEntity")
		{
			if (m_ActivedCamera == nullptr)
			{
				m_ActivedCamera = static_cast<ECameraEntity*>(InEntity);
			}
		}

		InEntity->OnSpawn();
	}

	void WScene::Start()
	{
		for (EEntity *Entity : m_Entities)
		{
			Entity->Start();
		}
	}

	void WScene::Update()
	{
		for (EEntity *Entity : m_Entities)
		{
			Entity->Update();
		}
	}

	void WScene::Exit()
	{
		for (EEntity *Entity : m_Entities)
		{
			Entity->Exit();
		}
	}

	WScene *WScene::GetScene()
	{
		return sCurrentScene;
	}
} // namespace WindmillEngine