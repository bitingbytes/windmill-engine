#pragma once

#include "Object/Object.h"
#include "Components/Components.h"

namespace WindmillEngine
{
	class EEntity : public WObject
	{
		WCLASS_DEF()
	public:
		EEntity() noexcept;
		~EEntity();

		/// <sumary>
		/// Called after the Entity was spawned by the Scene.
		/// </sumary>
		virtual void OnSpawn();

		/// <sumary>
		/// Called after the OnSpawn.
		/// </sumary>
		virtual void OnContruct();

		void Start();

		void Update();

		void Exit();

		template<class T>
		forceinline T* AddComponent()
		{
			return static_cast<T*>(AddComponentImp(new T()));
		}

		WComponent* AddComponent(WComponent* NewComponent);

		forceinline const std::set<WComponent*>& GetComponents() const
		{
			return m_Components;
		}

		template<class T>
		forceinline std::set<T*> GetComponents()
		{
			std::set<T*> Temp;
			for (WComponent* Component : m_Components)
			{
				if (T::TYPE == Component->GetType())
				{
					Temp.insert(static_cast<T*>(Component));
				}
			}

			return Temp;
		}

		template<class T>
		forceinline T* GetComponent()
		{
			for (WComponent* Component : m_Components)
			{
				if (T::TYPE == Component->GetType())
				{
					return static_cast<T*>(Component);
				}
			}

			return nullptr;
		}

		forceinline WTransformComponent* GetTransformComponent() const
		{
			return m_TransformComponent;
		}
	protected:
		std::set<WComponent*> m_Components;

	private:
		WComponent * AddComponentImp(WComponent* NewComponent);

		// Default Components
		WTransformComponent* m_TransformComponent;
	};
} // WindmillEngine