#include "WECorePCH.h"
#include "Entity.h"

namespace WindmillEngine
{
	WCLASS_DEC(EEntity, Entity, WObject);

	EEntity::EEntity() noexcept
		: WObject(TEXT("Entity"))
	{
		m_TransformComponent = AddComponent<WTransformComponent>();
	}

	EEntity::~EEntity()
	{
		for (WComponent* Component : m_Components)
		{
			delete Component;
		}
		m_Components.clear();
	}

	void EEntity::OnSpawn()
	{
		OnContruct();
	}

	void EEntity::OnContruct()
	{
		for (WComponent* Component : m_Components)
		{
			Component->InitializeComponent();
			Component->OnInitialize();
		}
	}

	void EEntity::Start()
	{
		for (WComponent* Component : m_Components)
		{
			Component->Start();
		}
	}

	void EEntity::Update()
	{
		for (WComponent* Component : m_Components)
		{
			Component->Update();
		}
	}

	void EEntity::Exit()
	{
		for (WComponent* Component : m_Components)
		{
			Component->Exit();
		}
	}

	WComponent* EEntity::AddComponent(WComponent * NewComponent)
	{
		return AddComponentImp(NewComponent);
	}

	WComponent* EEntity::AddComponentImp(WComponent* NewComponent)
	{
		if (NewComponent->GetOwner())
		{
			if (NewComponent->GetOwner() == this)
			{
				WE_WARN("Component %s is already assigned to %s.", NewComponent->GetName().c_str(), GetName().c_str());
			}
			else
			{
				WE_ERROR("Could not add component %s because it is owned by %s!!!", NewComponent->GetName().c_str(), NewComponent->GetOwner()->GetName().c_str());
			}
			return nullptr;
		}

		// TODO: Analizar isso
		/*for (WComponent* Component : m_Components)
		{
			if (Component != nullptr)
			{
				if (Component->IsExclusive())
				{
					WE_ERROR("Could not add component %s because it is excusive!!!");
					return nullptr;
				}
			}
		}*/

		if (!m_Components.insert(NewComponent).second)
		{
			WE_ERROR("Could not add Component %s to %s", NewComponent->GetName().c_str(), GetName().c_str());
			return nullptr;
		}

		NewComponent->SetOwner(this);
		NewComponent->OnAttach();

		return NewComponent;
	}
} // WindmillEngine