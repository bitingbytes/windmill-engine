#pragma once

#include "Entity.h"

namespace WindmillEngine
{
	class ECameraEntity : public EEntity
	{
		WCLASS_DEF()
	public:
		ECameraEntity() noexcept;
		~ECameraEntity();

		forceinline const WCameraComponent* GetCameraComponent() const
		{
			return m_CameraComponent;
		}

	private:
		WCameraComponent * m_CameraComponent;
	};
} // WindmillEngine