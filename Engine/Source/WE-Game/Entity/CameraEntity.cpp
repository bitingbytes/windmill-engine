#include "WECorePCH.h"
#include "CameraEntity.h"

namespace WindmillEngine
{
	WCLASS_DEC(ECameraEntity, CameraEntity, EEntity);

	ECameraEntity::ECameraEntity() noexcept
	{
		m_CameraComponent = AddComponent<WCameraComponent>();
	}

	ECameraEntity::~ECameraEntity()
	{
	}
} // WindmillEngine