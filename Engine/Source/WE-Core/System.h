// Copyright 2015-2017 Eremita Studios, Inc. All Rights Reserved.

#pragma once

#include <iostream>
#include <cstdarg>
#include <cstdio>
#include <vector>
#include <set>
#include <unordered_set>
#include <map>
#include <cmath>
#include <string>
#include <thread>
#include <mutex>
#include <atomic>
#include <algorithm>

#define BIT(x) 1u << x