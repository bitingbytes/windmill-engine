// <Eremita Copyrights>

#pragma once

#include "PIL/PlatformDetection.h"

#if defined(WE_EXPORT_DLL)
	#define CORE_API DLLEXPORT
#else
	#define CORE_API DLLIMPORT
#endif

