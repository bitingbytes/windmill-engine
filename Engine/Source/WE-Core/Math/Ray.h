// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct SRay
		{
			SVector3D Origin = { 0, 0, 0 };
			SVector3D Direction = { 0, 0, 0 };

			SRay() noexcept
			{
			}

			SRay(const SVector3D &InOrigin, const SVector3D &InDirection)
				: Origin(InOrigin), Direction(InDirection.Normalized())
			{
			}
		};
	} // namespace Math
} // namespace WindmillEngine