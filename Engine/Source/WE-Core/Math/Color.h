#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		template <class T, unsigned Max>
		struct CORE_API TColor
		{
			T R;
			T G;
			T B;
			T A;

			forceinline TColor() noexcept
				: R(0), G(0), B(0), A(Max)
			{
			}

			forceinline TColor(T InR, T InG, T InB, T InA = Max) noexcept
				: R(InR), G(InG), B(InB), A(InA)
			{
			}

			forceinline TColor operator+(const TColor<T, Max> &InLhs) const;

			forceinline TColor operator+(T InScale) const;

			forceinline TColor<T, Max> &operator+=(const TColor<T, Max> &InLhs);

			forceinline TColor<T, Max> &operator+=(T InScale);

			forceinline TColor operator-(const TColor<T, Max> &InLhs) const;

			forceinline TColor operator-(T InScale) const;

			forceinline TColor<T, Max> &operator-=(const TColor<T, Max> &InLhs);

			forceinline TColor<T, Max> &operator-=(T InScale);

			forceinline TColor operator*(const TColor<T, Max> &InLhs) const;

			forceinline TColor operator*(T InScale) const;

			forceinline TColor<T, Max> &operator*=(const TColor<T, Max> &InLhs);

			forceinline TColor<T, Max> &operator*=(T InScale);

			forceinline TColor operator/(const TColor<T, Max> &InLhs) const;

			forceinline TColor operator/(T InScale) const;

			forceinline TColor<T, Max> &operator/=(const TColor<T, Max> &InLhs);

			forceinline TColor<T, Max> &operator/=(T InScale);

			forceinline TColor operator-() const;

			forceinline T &operator[](uint32 InIndex);

			forceinline T operator[](uint32 InIndex) const;

			/*friend SArchive &operator<<(SArchive &InArquive, TColor &InColor)
			{
				return InArquive << InColor.R << InColor.G << InColor.B << InColor.A;
			}

			friend SArchive &operator>>(SArchive &InArquive, TColor &InColor)
			{
				return InArquive >> InColor.R >> InColor.G >> InColor.B >> InColor.A;
			}

			bool Serialize(SArchive &aArquive)
			{
				aArquive << *this;
				return true;
			}

			bool Deserialize(SArchive &aArquive)
			{
				aArquive >> R >> G >> B >> A;
				return true;
			}*/
		};

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator+(const TColor<T, Max> &InLhs) const
		{
			return TColor(R + InLhs.R, G + InLhs.G, B + InLhs.B);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator+(T InScale) const
		{
			return TColor(R + InScale, G + InScale, B + InScale);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator+=(const TColor<T, Max> &InLhs)
		{
			R += InLhs.R;
			G += InLhs.G;
			B += InLhs.B;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator+=(T InScale)
		{
			R += InScale;
			G += InScale;
			B += InScale;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator-(const TColor<T, Max> &InLhs) const
		{
			return TColor(R - InLhs.R, G - InLhs.G, B - InLhs.B);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator-(T InScale) const
		{
			return TColor(R - InScale, G - InScale, B - InScale);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator-=(const TColor<T, Max> &InLhs)
		{
			R -= InLhs.R;
			G -= InLhs.G;
			B -= InLhs.B;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator-=(T InScale)
		{
			R -= InScale;
			G -= InScale;
			B -= InScale;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator*(const TColor<T, Max> &InLhs) const
		{
			return TColor(R * InLhs.R, G * InLhs.G, B * InLhs.B);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator*(T InScale) const
		{
			return TColor(R * InScale, G * InScale, B * InScale);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator*=(const TColor<T, Max> &InLhs)
		{
			R *= InLhs.R;
			G *= InLhs.G;
			B *= InLhs.B;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator*=(T InScale)
		{
			R *= InScale;
			G *= InScale;
			B *= InScale;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator/(const TColor<T, Max> &InLhs) const
		{
			WE_ASSERT(InLhs.R != 0);
			WE_ASSERT(InLhs.G != 0);
			WE_ASSERT(InLhs.B != 0);

			return TColor(R / InLhs.R, G / InLhs.G, B / InLhs.B);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator/(T InScale) const
		{
			// WE_ASSERT(InScale != 0);

			return TColor(R / InScale, G / InScale, B / InScale);
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator/=(const TColor<T, Max> &InLhs)
		{
			WE_ASSERT(InLhs.R != 0);
			WE_ASSERT(InLhs.G != 0);
			WE_ASSERT(InLhs.B != 0);

			R /= InLhs.R;
			G /= InLhs.G;
			B /= InLhs.B;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> &TColor<T, Max>::operator/=(T InScale)
		{
			WE_ASSERT(InScale != 0);

			R /= InScale;
			G /= InScale;
			B /= InScale;

			return *this;
		}

		template <class T, unsigned Max>
		forceinline TColor<T, Max> TColor<T, Max>::operator-() const
		{
			return TColor(-R, -G, -B);
		}

		template <class T, unsigned Max>
		forceinline T &TColor<T, Max>::operator[](uint32 InIndex)
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 2);

			if (InIndex == 0)
				return R;
			else if (InIndex == 1)
				return G;
			else
				return B;
		}

		template <class T, unsigned Max>
		forceinline T TColor<T, Max>::operator[](uint32 InIndex) const
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 2);

			if (InIndex == 0)
				return R;
			else if (InIndex == 1)
				return G;
			else
				return B;
		}

		CORE_API typedef TColor<uint8, 255> SColor8;
		CORE_API typedef TColor<float, 1> SColor32;
		CORE_API typedef TColor<double, 1> SColor64;
	} // namespace Math
} // namespace WindmillEngine