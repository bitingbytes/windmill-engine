#include "MathLibrary.h"

namespace WindmillEngine
{
    namespace Math
    {
        const SVector2D SVector2D::Zero(0, 0);
        const SVector2D SVector2D::One(1, 1);
        const SVector2D SVector2D::Up(0, 1);
        const SVector2D SVector2D::Right(1, 0);

        const SVector3D SVector3D::Zero(0, 0, 0);
        const SVector3D SVector3D::One(1, 1, 1);
        const SVector3D SVector3D::Up(0, 0, 1);
        const SVector3D SVector3D::Forward(0, 1, 0);
        const SVector3D SVector3D::Right(1, 0, 0);

        const SVector4D SVector4D::Zero(0, 0, 0, 0);
        const SVector4D SVector4D::One(1, 1, 1, 1);
        const SVector4D SVector4D::Up(0, 0, 1, 0);
        const SVector4D SVector4D::Forward(0, 1, 0, 0);
        const SVector4D SVector4D::Right(1, 0, 0, 0); 
    } // namespace Math
} // namespace WindmillEngine