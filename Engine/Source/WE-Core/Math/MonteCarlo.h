// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		namespace MonteCarlo
		{
			forceinline static SVector4D UniformSampleHemisphere(const SVector2D& E)
			{
				float Phi = 2.0f * PI * E.X;
				float CosTheta = E.Y;
				float SinTheta = sqrt(1 - CosTheta * CosTheta);

				SVector3D H;
				H.X = SinTheta * cos(Phi);
				H.Y = SinTheta * sin(Phi);
				H.Z = CosTheta;

				float PDF = 1.0f / (2.0f * PI);

				return SVector4D(H, PDF);
			}

			forceinline static SMatrix4x4 GetTangentBasis(const SVector3D& TangentZ)
			{
				SVector3D UpVector = fabs(TangentZ.Z) < 0.999 ? SVector3D(0, 0, 1) : SVector3D(1, 0, 0);
				SVector3D TangentX = SVector3D::Cross(UpVector, TangentZ).Normalized();
				SVector3D TangentY = SVector3D::Cross(TangentZ, TangentX);
				return SMatrix4x4(TangentX, TangentY, TangentZ, SVector4D(0,0,0,1));
			}

			forceinline static SVector3D TangentToWorld(SVector3D Vec, SVector3D TangentZ)
			{
				return (SVector4D(Vec) * GetTangentBasis(TangentZ)).XYZ();
			}
		} // namespace MonteCarlo
	} // namespace Math
} // namespace WindmillEngine