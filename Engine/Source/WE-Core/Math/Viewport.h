// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct SViewport
		{
			uint32 Width;
			uint32 Height;

			SViewport()
				: Width(640)
				, Height(320)
			{}
		};
	}
}