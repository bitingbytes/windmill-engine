// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct SVector3D
		{
			float X;
			float Y;
			float Z;

			static CORE_API const SVector3D Zero;
			static CORE_API const SVector3D One;
			static CORE_API const SVector3D Up;
			static CORE_API const SVector3D Forward;
			static CORE_API const SVector3D Right;

			/* Constructors */
			forceinline SVector3D() = default;

			explicit forceinline SVector3D(float InXYZ)
				: X(InXYZ), Y(InXYZ), Z(InXYZ)
			{
			}

			forceinline SVector3D(float InX, float InY, float InZ = 0.0f)
				: X(InX), Y(InY), Z(InZ)
			{
			}

			explicit forceinline SVector3D(const SVector2D &Other, float InZ = 0.0f)
				: X(Other.X), Y(Other.Y), Z(InZ)
			{
			}

			forceinline SVector3D &operator=(const SVector3D &Other)
			{
				X = Other.X;
				Y = Other.Y;
				Z = Other.Z;

				return *this;
			}

			bool operator==(const SVector3D &Other) const;

			bool operator!=(const SVector3D &Other) const;

			bool operator<(const SVector3D &Other) const;

			bool operator>(const SVector3D &Other) const;

			bool operator<=(const SVector3D &Other) const;

			bool operator>=(const SVector3D &Other) const;

			/* Some Java users like to do operations like this. */
			forceinline SVector3D Add(const SVector3D &Other) const;

			forceinline SVector3D Sub(const SVector3D &Other) const;

			forceinline SVector3D Mul(const SVector3D &Other) const;

			forceinline SVector3D Div(const SVector3D &Other) const;

			forceinline SVector3D Add(float Other) const;

			forceinline SVector3D Sub(float Other) const;

			forceinline SVector3D Mul(float Other) const;

			forceinline SVector3D Div(float Other) const;

			forceinline float Dot(const SVector3D &InLhs) const;

			forceinline static float Dot(const SVector3D &InRhs, const SVector3D &InLhs)
			{
				return InRhs.Dot(InLhs);
			}

			forceinline SVector3D Cross(const SVector3D &InLhs) const;

			forceinline static SVector3D Cross(const SVector3D &InRhs, const SVector3D &InLhs)
			{
				return SVector3D(
					InRhs.Y * InLhs.Z - InRhs.Z * InLhs.Y,
					InRhs.Z * InLhs.X - InRhs.X * InLhs.Z,
					InRhs.X * InLhs.Y - InRhs.Y * InLhs.X);
			}

			forceinline SVector3D operator+(const SVector3D &InLhs) const;

			forceinline SVector3D operator+(float InScale) const;

			forceinline SVector3D &operator+=(const SVector3D &InLhs);

			forceinline SVector3D &operator+=(float InScale);

			forceinline SVector3D operator-(const SVector3D &InLhs) const;

			forceinline SVector3D operator-(float InScale) const;

			forceinline SVector3D &operator-=(const SVector3D &InLhs);

			forceinline SVector3D &operator-=(float InScale);

			forceinline SVector3D operator*(const SVector3D &InLhs) const;

			forceinline SVector3D operator*(float InScale) const;

			forceinline SVector3D &operator*=(const SVector3D &InLhs);

			forceinline SVector3D &operator*=(float InScale);

			forceinline SVector3D operator/(const SVector3D &InLhs) const;

			forceinline SVector3D operator/(float InScale) const;

			forceinline SVector3D &operator/=(const SVector3D &InLhs);

			forceinline SVector3D &operator/=(float InScale);

			forceinline SVector3D operator-() const;

			forceinline float &operator[](uint32 InIndex);

			forceinline float operator[](uint32 InIndex) const;

			forceinline float Length() const;

			forceinline static float Length(const SVector3D &InLhs)
			{
				return InLhs.Length();
			}

			forceinline SVector3D Normalized() const;

			forceinline static SVector3D Normalize(const SVector3D &InLhs)
			{
				return InLhs.Normalized();
			}

			forceinline float Angle(const SVector3D &InLhs) const;

			forceinline static float Angle(const SVector3D &InRhs, const SVector3D &InLhs)
			{
				return InRhs.Angle(InLhs);
			}

			forceinline float Distance(const SVector3D &InLhs) const;

			forceinline static float Distance(const SVector3D &InRhs, const SVector3D &InLhs)
			{
				return InRhs.Distance(InLhs);
			}

			forceinline SVector3D Projection(const SVector3D &InLhs) const;

			forceinline static SVector3D Projection(const SVector3D &InRhs, const SVector3D &InLhs)
			{
				return InRhs.Projection(InLhs);
			}

			forceinline SVector3D Reflect(const SVector3D &InNormal) const;

			forceinline static SVector3D Reflect(const SVector3D &InIncidence, const SVector3D &InNormal)
			{
				return InIncidence.Reflect(InNormal);
			}

			forceinline SVector3D Refract(const SVector3D &InNormal, float InEta) const;

			forceinline static SVector3D Refract(const SVector3D &InIncidence, const SVector3D &InNormal, float InEta)
			{
				return InIncidence.Refract(InNormal, InEta);
			}

			forceinline SVector3D XZY() const
			{
				return SVector3D(X, Z, Y);
			}

			/*friend SArchive &operator<<(SArchive &InArquive, SVector3D &InVector)
			{
				return InArquive << InVector.X << InVector.Y << InVector.Z;
			}

			friend SArchive &operator>>(SArchive &InArquive, SVector3D &InVector)
			{
				return InArquive >> InVector.X >> InVector.Y >> InVector.Z;
			}

			bool Serialize(SArchive &aArquive)
			{
				aArquive << *this;
				return true;
			}

			bool Deserialize(SArchive &aArquive)
			{
				aArquive >> X >> Y >> Z;
				return true;
			}*/

		};

		forceinline bool SVector3D::operator==(const SVector3D &Other) const
		{
			return X == Other.X &&
				Y == Other.Y &&
				Z == Other.Z;
		}

		forceinline bool SVector3D::operator!=(const SVector3D &Other) const
		{
			return X == Other.X ||
				Y == Other.Y ||
				Z == Other.Z;
		}

		forceinline bool SVector3D::operator<(const SVector3D &Other) const
		{
			return Length() < Other.Length();
		}

		forceinline bool SVector3D::operator>(const SVector3D &Other) const
		{
			return Length() > Other.Length();
		}

		forceinline bool SVector3D::operator<=(const SVector3D &Other) const
		{
			return Length() <= Other.Length();
		}

		forceinline bool SVector3D::operator>=(const SVector3D &Other) const
		{
			return Length() >= Other.Length();
		}

		forceinline SVector3D operator*(float InScale, const SVector3D &InVector)
		{
			return InVector.operator*(InScale);
		}

		forceinline SVector3D SVector3D::Add(const SVector3D &Other) const
		{
			return SVector3D(X + Other.X, Y + Other.Y, Z + Other.Z);
		}

		forceinline SVector3D SVector3D::Sub(const SVector3D &Other) const
		{
			return SVector3D(X - Other.X, Y - Other.Y, Z - Other.Z);
		}

		forceinline SVector3D SVector3D::Mul(const SVector3D &Other) const
		{
			return SVector3D(X * Other.X, Y * Other.Y, Z * Other.Z);
		}

		forceinline SVector3D SVector3D::Div(const SVector3D &Other) const
		{
			WE_ASSERT(Other.X != 0);
			WE_ASSERT(Other.Y != 0);
			WE_ASSERT(Other.Z != 0);

			return SVector3D(X / Other.X, Y / Other.Y, Z / Other.Z);
		}

		forceinline SVector3D SVector3D::Add(float Other) const
		{
			return SVector3D(X + Other, Y + Other, Z + Other);
		}

		forceinline SVector3D SVector3D::Sub(float Other) const
		{
			return SVector3D(X - Other, Y - Other, Z - Other);
		}

		forceinline SVector3D SVector3D::Mul(float Other) const
		{
			return SVector3D(X * Other, Y * Other, Z * Other);
		}

		forceinline SVector3D SVector3D::Div(float Other) const
		{
			WE_ASSERT(Other != 0);

			return SVector3D(X / Other, Y / Other, Z / Other);
		}

		forceinline float SVector3D::Dot(const SVector3D &InLhs) const
		{
			return X * InLhs.X + Y * InLhs.Y + Z * InLhs.Z;
		}

		forceinline SVector3D SVector3D::Cross(const SVector3D &InLhs) const
		{
			return SVector3D(
				Y * InLhs.Z - Z * InLhs.Y,
				Z * InLhs.X - X * InLhs.Z,
				X * InLhs.Y - Y * InLhs.X);
		}

		forceinline SVector3D SVector3D::operator+(const SVector3D &InLhs) const
		{
			return SVector3D(X + InLhs.X, Y + InLhs.Y, Z + InLhs.Z);
		}

		forceinline SVector3D SVector3D::operator+(float InScale) const
		{
			return SVector3D(X + InScale, Y + InScale, Z + InScale);
		}

		forceinline SVector3D &SVector3D::operator+=(const SVector3D &InLhs)
		{
			X += InLhs.X;
			Y += InLhs.Y;
			Z += InLhs.Z;

			return *this;
		}

		forceinline SVector3D &SVector3D::operator+=(float InScale)
		{
			X += InScale;
			Y += InScale;
			Z += InScale;

			return *this;
		}

		forceinline SVector3D SVector3D::operator-(const SVector3D &InLhs) const
		{
			return SVector3D(X - InLhs.X, Y - InLhs.Y, Z - InLhs.Z);
		}

		forceinline SVector3D SVector3D::operator-(float InScale) const
		{
			return SVector3D(X - InScale, Y - InScale, Z - InScale);
		}

		forceinline SVector3D &SVector3D::operator-=(const SVector3D &InLhs)
		{
			X -= InLhs.X;
			Y -= InLhs.Y;
			Z -= InLhs.Z;

			return *this;
		}

		forceinline SVector3D &SVector3D::operator-=(float InScale)
		{
			X -= InScale;
			Y -= InScale;
			Z -= InScale;

			return *this;
		}

		forceinline SVector3D SVector3D::operator*(const SVector3D &InLhs) const
		{
			return SVector3D(X * InLhs.X, Y * InLhs.Y, Z * InLhs.Z);
		}

		forceinline SVector3D SVector3D::operator*(float InScale) const
		{
			return SVector3D(X * InScale, Y * InScale, Z * InScale);
		}

		forceinline SVector3D &SVector3D::operator*=(const SVector3D &InLhs)
		{
			X *= InLhs.X;
			Y *= InLhs.Y;
			Z *= InLhs.Z;

			return *this;
		}

		forceinline SVector3D &SVector3D::operator*=(float InScale)
		{
			X *= InScale;
			Y *= InScale;
			Z *= InScale;

			return *this;
		}

		forceinline SVector3D SVector3D::operator/(const SVector3D &InLhs) const
		{
			WE_ASSERT(InLhs.X != 0);
			WE_ASSERT(InLhs.Y != 0);
			WE_ASSERT(InLhs.Z != 0);

			return SVector3D(X / InLhs.X, Y / InLhs.Y, Z / InLhs.Z);
		}

		forceinline SVector3D SVector3D::operator/(float InScale) const
		{
			// WE_ASSERT(InScale != 0);

			return SVector3D(X / InScale, Y / InScale, Z / InScale);
		}

		forceinline SVector3D &SVector3D::operator/=(const SVector3D &InLhs)
		{
			WE_ASSERT(InLhs.X != 0);
			WE_ASSERT(InLhs.Y != 0);
			WE_ASSERT(InLhs.Z != 0);

			X /= InLhs.X;
			Y /= InLhs.Y;
			Z /= InLhs.Z;

			return *this;
		}

		forceinline SVector3D &SVector3D::operator/=(float InScale)
		{
			WE_ASSERT(InScale != 0);

			X /= InScale;
			Y /= InScale;
			Z /= InScale;

			return *this;
		}

		forceinline SVector3D SVector3D::operator-() const
		{
			return SVector3D(-X, -Y, -Z);
		}

		forceinline float &SVector3D::operator[](uint32 InIndex)
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 2);

			if (InIndex == 0)
				return X;
			else if (InIndex == 1)
				return Y;
			else
				return Z;
		}

		forceinline float SVector3D::operator[](uint32 InIndex) const
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 2);

			if (InIndex == 0)
				return X;
			else if (InIndex == 1)
				return Y;
			else
				return Z;
		}

		forceinline float SVector3D::Length() const
		{
			return Sqrt(Square(X) + Square(Y) + Square(Z));
		}

		forceinline SVector3D SVector3D::Normalized() const
		{
			float Length = this->Length();
			return *this / Length;
		}

		forceinline float SVector3D::Angle(const SVector3D &InLhs) const
		{
			float Dot = this->Dot(InLhs);
			float LengthProduct = this->Length() * InLhs.Length();
			float Cos = Dot / LengthProduct;

			return ACos(Cos);
		}

		forceinline float SVector3D::Distance(const SVector3D &InLhs) const
		{
			return (InLhs - *this).Length();
		}

		forceinline SVector3D SVector3D::Projection(const SVector3D &InLhs) const
		{
			float Dot = this->Dot(InLhs);
			float Length = this->Length();

			return this->Normalized() * (Dot / Length);
		}

		forceinline SVector3D SVector3D::Reflect(const SVector3D &InNormal) const
		{
			return *this - 2.0f * InNormal * InNormal.Dot(*this);
		}

		forceinline SVector3D SVector3D::Refract(const SVector3D &InNormal, float InEta) const
		{
			float CosI = -(*this).Dot(InNormal);
			float Cost2 = 1.0f - Sqrt(InEta) * (1.0f - Square(CosI));
			SVector3D T = InEta * (*this) + ((InEta * CosI - Sqrt(Abs(Cost2))) * InNormal);
			return T * (SVector3D)(Cost2 > 0);
		}

		forceinline static SVector3D Max(const SVector3D &InA, const SVector3D &InB)
		{
			return InA > InB ? InA : InB;
		}

		forceinline static SVector3D Min(const SVector3D &InA, const SVector3D &InB)
		{
			return InA < InB ? InA : InB;
		}

		forceinline static SVector3D Clamp(const SVector3D &InMin, const SVector3D &InMax, const SVector3D &InValue)
		{
			return Max(InMin, Min(InMax, InValue));
		}

		forceinline static SVector3D Saturate(const SVector3D &InValue)
		{
			return Clamp(SVector3D::Zero, SVector3D::One, InValue);
		}

		forceinline static SVector3D Lerp(const SVector3D &InA, const SVector3D &InB, float InDelta)
		{
			return SVector3D(
				((1.0f - InDelta) * InA.X + InB.X * InDelta),
				((1.0f - InDelta) * InA.Y + InB.Y * InDelta),
				((1.0f - InDelta) * InA.Z + InB.Z * InDelta));
		}

	} // namespace Math
} // namespace WindmillEngine