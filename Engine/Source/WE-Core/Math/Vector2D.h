// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct SVector2D
		{
			float X;
			float Y;

			static CORE_API const SVector2D Zero;
			static CORE_API const SVector2D One;
			static CORE_API const SVector2D Up;
			static CORE_API const SVector2D Right;

			/* Constructors */
			forceinline SVector2D() = default;

			forceinline explicit SVector2D(float InXY)
				: X(InXY), Y(InXY)
			{
			}

			forceinline SVector2D(float InX, float InY)
				: X(InX), Y(InY)
			{
			}

			forceinline SVector2D &operator=(const SVector2D &Other)
			{
				X = Other.X;
				Y = Other.Y;

				return *this;
			}

			bool operator==(const SVector2D &Other) const;

			bool operator!=(const SVector2D &Other) const;

			bool operator<(const SVector2D &Other) const;

			bool operator>(const SVector2D &Other) const;

			bool operator<=(const SVector2D &Other) const;

			bool operator>=(const SVector2D &Other) const;

			/* Some Java users like to do operations like this. */
			forceinline SVector2D Add(const SVector2D &Other) const;

			forceinline SVector2D Sub(const SVector2D &Other) const;

			forceinline SVector2D Mul(const SVector2D &Other) const;

			forceinline SVector2D Div(const SVector2D &Other) const;

			forceinline SVector2D Add(float Other) const;

			forceinline SVector2D Sub(float Other) const;

			forceinline SVector2D Mul(float Other) const;

			forceinline SVector2D Div(float Other) const;

			forceinline float Dot(const SVector2D &InLhs) const;

			forceinline static float Dot(const SVector2D &InRhs, const SVector2D &InLhs)
			{
				return InRhs.Dot(InLhs);
			}

			forceinline SVector2D operator+(const SVector2D &InLhs) const;

			forceinline SVector2D operator+(float InScale) const;

			forceinline SVector2D &operator+=(const SVector2D &InLhs);

			forceinline SVector2D &operator+=(float InScale);

			forceinline SVector2D operator-(const SVector2D &InLhs) const;

			forceinline SVector2D operator-(float InScale) const;

			forceinline SVector2D &operator-=(const SVector2D &InLhs);

			forceinline SVector2D &operator-=(float InScale);

			forceinline SVector2D operator*(const SVector2D &InLhs) const;

			forceinline SVector2D operator*(float InScale) const;

			forceinline SVector2D &operator*=(const SVector2D &InLhs);

			forceinline SVector2D &operator*=(float InScale);

			forceinline SVector2D operator/(const SVector2D &InLhs) const;

			forceinline SVector2D operator/(float InScale) const;

			forceinline SVector2D &operator/=(const SVector2D &InLhs);

			forceinline SVector2D &operator/=(float InScale);

			forceinline SVector2D operator-() const;

			forceinline float &operator[](uint32 InIndex);

			forceinline float operator[](uint32 InIndex) const;

			forceinline float Length() const;

			forceinline static float Length(const SVector2D &InLhs)
			{
				return InLhs.Length();
			}

			forceinline SVector2D Normalized() const;

			forceinline static SVector2D Normalize(const SVector2D &InLhs)
			{
				return InLhs.Normalized();
			}

			forceinline float Angle(const SVector2D &InLhs) const;

			forceinline static float Angle(const SVector2D &InRhs, const SVector2D &InLhs)
			{
				return InRhs.Angle(InLhs);
			}

			forceinline float Distance(const SVector2D &InLhs) const;

			forceinline static float Distance(const SVector2D &InRhs, const SVector2D &InLhs)
			{
				return InRhs.Distance(InLhs);
			}

			forceinline SVector2D Projection(const SVector2D &InLhs) const;

			forceinline static SVector2D Projection(const SVector2D &InRhs, const SVector2D &InLhs)
			{
				return InRhs.Projection(InLhs);
			}

			forceinline SVector2D Reflect(const SVector2D &InNormal) const;

			forceinline static SVector2D Reflect(const SVector2D &InIncidence, const SVector2D &InNormal)
			{
				return InIncidence.Reflect(InNormal);
			}

			forceinline SVector2D Refract(const SVector2D &InNormal, float InEta) const;

			forceinline static SVector2D Refract(const SVector2D &InIncidence, const SVector2D &InNormal, float InEta)
			{
				return InIncidence.Refract(InNormal, InEta);
			}

			/*friend SArchive &operator<<(SArchive &InArquive, SVector2D &InVector)
			{
				return InArquive << InVector.X << InVector.Y;
			}

			friend SArchive &operator>>(SArchive &InArquive, SVector2D &InVector)
			{
				return InArquive >> InVector.X >> InVector.Y;
			}

			bool Serialize(SArchive &aArquive)
			{
				aArquive << *this;
				return true;
			}

			bool Deserialize(SArchive &aArquive)
			{
				aArquive >> X >> Y;
				return true;
			}*/
		};

		forceinline bool SVector2D::operator==(const SVector2D &Other) const
		{
			return X == Other.X &&
				Y == Other.Y;
		}

		forceinline bool SVector2D::operator!=(const SVector2D &Other) const
		{
			return X == Other.X ||
				Y == Other.Y;
		}

		forceinline bool SVector2D::operator<(const SVector2D &Other) const
		{
			return Length() < Other.Length();
		}

		forceinline bool SVector2D::operator>(const SVector2D &Other) const
		{
			return Length() > Other.Length();
		}

		forceinline bool SVector2D::operator<=(const SVector2D &Other) const
		{
			return Length() <= Other.Length();
		}

		forceinline bool SVector2D::operator>=(const SVector2D &Other) const
		{
			return Length() >= Other.Length();
		}

		forceinline SVector2D operator*(float InScale, const SVector2D &InVector)
		{
			return InVector.operator*(InScale);
		}

		forceinline SVector2D SVector2D::Add(const SVector2D &Other) const
		{
			return SVector2D(X + Other.X, Y + Other.Y);
		}

		forceinline SVector2D SVector2D::Sub(const SVector2D &Other) const
		{
			return SVector2D(X - Other.X, Y - Other.Y);
		}

		forceinline SVector2D SVector2D::Mul(const SVector2D &Other) const
		{
			return SVector2D(X * Other.X, Y * Other.Y);
		}

		forceinline SVector2D SVector2D::Div(const SVector2D &Other) const
		{
			WE_ASSERT(Other.X != 0);
			WE_ASSERT(Other.Y != 0);

			return SVector2D(X / Other.X, Y / Other.Y);
		}

		forceinline SVector2D SVector2D::Add(float Other) const
		{
			return SVector2D(X + Other, Y + Other);
		}

		forceinline SVector2D SVector2D::Sub(float Other) const
		{
			return SVector2D(X - Other, Y - Other);
		}

		forceinline SVector2D SVector2D::Mul(float Other) const
		{
			return SVector2D(X * Other, Y * Other);
		}

		forceinline SVector2D SVector2D::Div(float Other) const
		{
			WE_ASSERT(Other != 0);

			return SVector2D(X / Other, Y / Other);
		}

		forceinline float SVector2D::Dot(const SVector2D &InLhs) const
		{
			return X * InLhs.X + Y * InLhs.Y;
		}

		forceinline SVector2D SVector2D::operator+(const SVector2D &InLhs) const
		{
			return SVector2D(X + InLhs.X, Y + InLhs.Y);
		}

		forceinline SVector2D SVector2D::operator+(float InScale) const
		{
			return SVector2D(X + InScale, Y + InScale);
		}

		forceinline SVector2D &SVector2D::operator+=(const SVector2D &InLhs)
		{
			X += InLhs.X;
			Y += InLhs.Y;

			return *this;
		}

		forceinline SVector2D &SVector2D::operator+=(float InScale)
		{
			X += InScale;
			Y += InScale;

			return *this;
		}

		forceinline SVector2D SVector2D::operator-(const SVector2D &InLhs) const
		{
			return SVector2D(X - InLhs.X, Y - InLhs.Y);
		}

		forceinline SVector2D SVector2D::operator-(float InScale) const
		{
			return SVector2D(X - InScale, Y - InScale);
		}

		forceinline SVector2D &SVector2D::operator-=(const SVector2D &InLhs)
		{
			X -= InLhs.X;
			Y -= InLhs.Y;

			return *this;
		}

		forceinline SVector2D &SVector2D::operator-=(float InScale)
		{
			X -= InScale;
			Y -= InScale;

			return *this;
		}

		forceinline SVector2D SVector2D::operator*(const SVector2D &InLhs) const
		{
			return SVector2D(X * InLhs.X, Y * InLhs.Y);
		}

		forceinline SVector2D SVector2D::operator*(float InScale) const
		{
			return SVector2D(X * InScale, Y * InScale);
		}

		forceinline SVector2D &SVector2D::operator*=(const SVector2D &InLhs)
		{
			X *= InLhs.X;
			Y *= InLhs.Y;

			return *this;
		}

		forceinline SVector2D &SVector2D::operator*=(float InScale)
		{
			X *= InScale;
			Y *= InScale;

			return *this;
		}

		forceinline SVector2D SVector2D::operator/(const SVector2D &InLhs) const
		{
			WE_ASSERT(InLhs.X != 0);
			WE_ASSERT(InLhs.Y != 0);

			return SVector2D(X / InLhs.X, Y / InLhs.Y);
		}

		forceinline SVector2D SVector2D::operator/(float InScale) const
		{
			WE_ASSERT(InScale != 0);

			return SVector2D(X / InScale, Y / InScale);
		}

		forceinline SVector2D &SVector2D::operator/=(const SVector2D &InLhs)
		{
			WE_ASSERT(InLhs.X != 0);
			WE_ASSERT(InLhs.Y != 0);

			X /= InLhs.X;
			Y /= InLhs.Y;

			return *this;
		}

		forceinline SVector2D &SVector2D::operator/=(float InScale)
		{
			WE_ASSERT(InScale != 0);

			X /= InScale;
			Y /= InScale;

			return *this;
		}

		forceinline SVector2D SVector2D::operator-() const
		{
			return SVector2D(-X, -Y);
		}

		forceinline float &SVector2D::operator[](uint32 InIndex)
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 2);

			if (InIndex == 0)
				return X;
			else
				return Y;
		}

		forceinline float SVector2D::operator[](uint32 InIndex) const
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 2);

			if (InIndex == 0)
				return X;
			else
				return Y;
		}

		forceinline float SVector2D::Length() const
		{
			return Sqrt(Square(X) + Square(Y));
		}

		forceinline SVector2D SVector2D::Normalized() const
		{
			float Length = this->Length();
			return *this / Length;
		}

		forceinline float SVector2D::Angle(const SVector2D &InLhs) const
		{
			float Dot = this->Dot(InLhs);
			float LengthProduct = this->Length() * InLhs.Length();
			float Cos = Dot / LengthProduct;

			return ACos(Cos);
		}

		forceinline float SVector2D::Distance(const SVector2D &InLhs) const
		{
			return (InLhs - *this).Length();
		}

		forceinline SVector2D SVector2D::Projection(const SVector2D &InLhs) const
		{
			float Dot = this->Dot(InLhs);
			float Length = this->Length();

			return this->Normalized() * (Dot / Length);
		}

		forceinline SVector2D SVector2D::Reflect(const SVector2D &InNormal) const
		{
			return *this - 2.0f * InNormal * InNormal.Dot(*this);
		}

		forceinline SVector2D SVector2D::Refract(const SVector2D &InNormal, float InEta) const
		{
			float CosI = -(*this).Dot(InNormal);
			float Cost2 = 1.0f - Sqrt(InEta) * (1.0f - Square(CosI));
			SVector2D T = InEta * (*this) + ((InEta * CosI - Sqrt(Abs(Cost2))) * InNormal);
			return T * (SVector2D)(Cost2 > 0);
		}

		forceinline static SVector2D Max(const SVector2D &InA, const SVector2D &InB)
		{
			return InA > InB ? InA : InB;
		}

		forceinline static SVector2D Min(const SVector2D &InA, const SVector2D &InB)
		{
			return InA < InB ? InA : InB;
		}

		forceinline static SVector2D Clamp(const SVector2D &InMin, const SVector2D &InMax, const SVector2D &InValue)
		{
			return Max(InMin, Min(InMax, InValue));
		}

		forceinline static SVector2D Saturate(const SVector2D &InValue)
		{
			return Clamp(SVector2D::Zero, SVector2D::One, InValue);
		}

		forceinline static SVector2D Lerp(const SVector2D &InA, const SVector2D &InB, float InDelta)
		{
			return SVector2D(
				((1.0f - InDelta) * InA.X + InB.X * InDelta),
				((1.0f - InDelta) * InA.Y + InB.Y * InDelta));
		}
	} // namespace Math
} // namespace WindmillEngine