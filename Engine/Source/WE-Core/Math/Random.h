// <Eremita Copyrights>

#pragma once

#include "Math.h"
#include "Vector3D.h"
#include <random>
#include <ctime>

namespace WindmillEngine
{
	namespace Math
	{
		namespace Random
		{
			forceinline static float Range(float InMin, float InMax)
			{
				std::random_device rd;  //Will be used to obtain a seed for the random number engine
				std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
				std::uniform_real_distribution<float> dis(InMin, InMax);
				return dis(gen);
			}

			forceinline static SVector3D InsideSphere()
			{
				float Radius = Range(0.0f, 1.0f);
				float Phi = 2.0f * PI * Range(0.0f, 1.0f);
				float CosTheta = 1.0f - 2.0f * Range(0.0f, 1.0f);
				float SinTheta = Sqrt(1.0f - CosTheta * CosTheta);

				SVector3D Temp;
				Temp.X = SinTheta * cos(Phi);
				Temp.Y = SinTheta * sin(Phi);
				Temp.Z = CosTheta;

				return Radius * Temp;
			}
		} // namespace Random
	} // namespace Math
} // namespace WindmillEngine