// <Eremita Copyrights>

#pragma once

#include "Math.h"
#include "Vector3D.h"
#include <cstring>

namespace WindmillEngine
{
	namespace Math
	{
		struct CORE_API SMatrix3x3
		{
			union
			{
				float E[3][3] = { { 0 } };
				SVector3D Row[3];
			};

			forceinline SMatrix3x3() noexcept
			{

			}

			forceinline SMatrix3x3(float InDi);

			forceinline SMatrix3x3(float InE00, float InE01, float InE02,
				float InE10, float InE11, float InE12,
				float InE20, float InE21, float InE22);

			forceinline SMatrix3x3(const SVector3D& InRow0, const SVector3D& InRow1, const SVector3D& InRow2);

			forceinline SMatrix3x3 operator*(const SMatrix3x3& Other) const;

			forceinline void operator*=(const SMatrix3x3& Other);

			forceinline SMatrix3x3 operator*(float InScalar) const;

			forceinline SMatrix3x3 operator+(const SMatrix3x3& Other) const;

			forceinline SMatrix3x3& operator+=(const SMatrix3x3& Other);

			forceinline SMatrix3x3 operator+(float InScalar) const;

			forceinline static SMatrix3x3 Translate(const SVector2D& InTranslation)
			{
				return SMatrix3x3(
					1.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f,
					InTranslation.X, InTranslation.Y, 1.0f);
			}

			forceinline static SMatrix3x3 Rotate(float InAngle)
			{
				return SMatrix3x3(
					Cos(InAngle), Sin(InAngle), 0.0f,
					-Sin(InAngle), Cos(InAngle), 0.0f,
					0.0f, 0.0f, 1.0f
				);
			}

			forceinline static SMatrix3x3 Scale(const SVector2D& InScale)
			{
				return SMatrix3x3(
					InScale.X, 0.0f, 0.0f,
					0.0f, InScale.Y, 0.0f,
					0.0f, 0.0f, 1.0f
				);
			}
		};

		SMatrix3x3::SMatrix3x3(float InDi)
		{
			E[0][0] = InDi; E[0][1] = 0.0f; E[0][2] = 0.0f;
			E[1][0] = 0.0f; E[1][1] = InDi; E[1][2] = 0.0f;
			E[2][0] = 0.0f; E[2][1] = 0.0f; E[2][2] = InDi;
		}

		SMatrix3x3::SMatrix3x3(float InE00, float InE01, float InE02,
			float InE10, float InE11, float InE12,
			float InE20, float InE21, float InE22)
		{
			E[0][0] = InE00; E[0][1] = InE01; E[0][2] = InE02;
			E[1][0] = InE10; E[1][1] = InE11; E[1][2] = InE12;
			E[2][0] = InE20; E[2][1] = InE21; E[2][2] = InE22;
		}

		SMatrix3x3::SMatrix3x3(const SVector3D& InRow0, const SVector3D& InRow1, const SVector3D& InRow2)
		{
			E[0][0] = InRow0.X; E[0][1] = InRow0.Y; E[0][2] = InRow0.Z;
			E[1][0] = InRow1.X; E[1][1] = InRow1.Y; E[1][2] = InRow1.Z;
			E[2][0] = InRow2.X; E[2][1] = InRow2.Y; E[2][2] = InRow2.Z;
		}

		forceinline static void MatrixMultiplicator(SMatrix3x3* OutDestination, const SMatrix3x3* InSource, const SMatrix3x3* InOther)
		{
			SMatrix3x3 Temp = SMatrix3x3(0.0f);

			SVector3D Row0 = SVector3D(InSource->E[0][0], InSource->E[0][1], InSource->E[0][2]);
			SVector3D Row1 = SVector3D(InSource->E[1][0], InSource->E[1][1], InSource->E[1][2]);
			SVector3D Row2 = SVector3D(InSource->E[2][0], InSource->E[2][1], InSource->E[2][2]);

			SVector3D Col0 = SVector3D(InOther->E[0][0], InOther->E[1][0], InOther->E[2][0]);
			SVector3D Col1 = SVector3D(InOther->E[0][1], InOther->E[1][1], InOther->E[2][1]);
			SVector3D Col2 = SVector3D(InOther->E[0][2], InOther->E[1][2], InOther->E[2][2]);

			Temp.E[0][0] = Row0.Dot(Col0); Temp.E[0][1] = Row0.Dot(Col1); Temp.E[0][2] = Row0.Dot(Col2);
			Temp.E[1][0] = Row1.Dot(Col0); Temp.E[1][1] = Row1.Dot(Col1); Temp.E[1][2] = Row1.Dot(Col2);
			Temp.E[2][0] = Row2.Dot(Col0); Temp.E[2][1] = Row2.Dot(Col1); Temp.E[2][2] = Row2.Dot(Col2);

			memcpy(OutDestination, &Temp, 9 * sizeof(float));
		}

		forceinline SVector2D operator*(const SVector2D& InVector, const SMatrix3x3& InMatrix)
		{
			SVector3D Vector = SVector3D(InVector.X, InVector.Y, 1.0f);
			SVector3D Col0 = SVector3D(InMatrix.E[0][0], InMatrix.E[1][0], InMatrix.E[2][0]);
			SVector3D Col1 = SVector3D(InMatrix.E[0][1], InMatrix.E[1][1], InMatrix.E[2][1]);

			float TempX = Vector.Dot(Col0);
			float TempY = Vector.Dot(Col1);

			return SVector2D(TempX, TempY);
		}

		SMatrix3x3 SMatrix3x3::operator*(const SMatrix3x3& Other) const
		{
			SMatrix3x3 Temp = SMatrix3x3(0.0f);

			MatrixMultiplicator(&Temp, this, &Other);

			return Temp;
		}

		void SMatrix3x3::operator*=(const SMatrix3x3& Other)
		{
			MatrixMultiplicator(this, this, &Other);
		}

		SMatrix3x3 SMatrix3x3::operator*(float InScalar) const
		{
			return SMatrix3x3(
				Row[0] * InScalar,
				Row[1] * InScalar,
				Row[2] * InScalar
			);
		}

		SMatrix3x3 SMatrix3x3::operator+(const SMatrix3x3& Other) const
		{
			return SMatrix3x3(
				Row[0] + Other.Row[0],
				Row[1] + Other.Row[1],
				Row[2] + Other.Row[2]
			);
		}

		SMatrix3x3& SMatrix3x3::operator+=(const SMatrix3x3& Other)
		{
			Row[0] += Other.Row[0];
			Row[1] += Other.Row[1];
			Row[2] += Other.Row[2];

			return *this;
		}

		SMatrix3x3 SMatrix3x3::operator+(float InScalar) const
		{
			return SMatrix3x3(
				Row[0] + InScalar,
				Row[1] + InScalar,
				Row[2] + InScalar
			);
		}
	} // namespace Math
} // namespace WindmillEngine