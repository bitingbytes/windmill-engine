// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct ALIGN(16) SVector4D
		{
			float X;
			float Y;
			float Z;
			float W;

			static CORE_API const SVector4D Zero;
			static CORE_API const SVector4D One;
			static CORE_API const SVector4D Up;
			static CORE_API const SVector4D Forward;
			static CORE_API const SVector4D Right;

			/* Constructors */
			SVector4D() = default;

			explicit forceinline SVector4D(float InXYZW)
				: X(InXYZW), Y(InXYZW), Z(InXYZW), W(InXYZW)
			{
			}

			forceinline SVector4D(float InX, float InY, float InZ, float InW = 0.0f)
				: X(InX), Y(InY), Z(InZ), W(InW)
			{
			}

			forceinline SVector4D(const SVector3D &Other, float InW = 1.0f)
				: X(Other.X), Y(Other.Y), Z(Other.Z), W(InW)
			{
			}

			forceinline SVector4D &operator=(const SVector4D &Other)
			{
				X = Other.X;
				Y = Other.Y;
				Z = Other.Z;
				W = Other.W;

				return *this;
			}

			bool operator==(const SVector4D &Other) const;

			bool operator!=(const SVector4D &Other) const;

			bool operator<(const SVector4D &Other) const;

			bool operator>(const SVector4D &Other) const;

			bool operator<=(const SVector4D &Other) const;

			bool operator>=(const SVector4D &Other) const;

			/* Some Java users like to do operations like this. */
			forceinline SVector4D Add(const SVector4D &Other) const;

			forceinline SVector4D Sub(const SVector4D &Other) const;

			forceinline SVector4D Mul(const SVector4D &Other) const;

			forceinline SVector4D Div(const SVector4D &Other) const;

			forceinline SVector4D Add(float Other) const;

			forceinline SVector4D Sub(float Other) const;

			forceinline SVector4D Mul(float Other) const;

			forceinline SVector4D Div(float Other) const;

			forceinline float Dot(const SVector4D &InLhs) const;

			forceinline static float Dot(const SVector4D &InRhs, const SVector4D &InLhs)
			{
				return InRhs.Dot(InLhs);
			}

			forceinline SVector4D operator+(const SVector4D &InLhs) const;

			forceinline SVector4D operator+(float InScale) const;

			forceinline SVector4D &operator+=(const SVector4D &InLhs);

			forceinline SVector4D &operator+=(float InScale);

			forceinline SVector4D operator-(const SVector4D &InLhs) const;

			forceinline SVector4D operator-(float InScale) const;

			forceinline SVector4D &operator-=(const SVector4D &InLhs);

			forceinline SVector4D &operator-=(float InScale);

			forceinline SVector4D operator*(const SVector4D &InLhs) const;

			forceinline SVector4D operator*(float InScale) const;

			forceinline SVector4D &operator*=(const SVector4D &InLhs);

			forceinline SVector4D &operator*=(float InScale);

			forceinline SVector4D operator/(const SVector4D &InLhs) const;

			forceinline SVector4D operator/(float InScale) const;

			forceinline SVector4D &operator/=(const SVector4D &InLhs);

			forceinline SVector4D &operator/=(float InScale);

			forceinline SVector4D operator-() const;

			forceinline float &operator[](uint32 InIndex);

			forceinline float operator[](uint32 InIndex) const;

			forceinline float Length() const;

			forceinline static float Length(const SVector4D &InLhs)
			{
				return InLhs.Length();
			}

			forceinline SVector4D Normalized() const;

			forceinline static SVector4D Normalize(const SVector4D &InLhs)
			{
				return InLhs.Normalized();
			}

			forceinline float Angle(const SVector4D &InLhs) const;

			forceinline static float Angle(const SVector4D &InRhs, const SVector4D &InLhs)
			{
				return InRhs.Angle(InLhs);
			}

			forceinline float Distance(const SVector4D &InLhs) const;

			forceinline static float Distance(const SVector4D &InRhs, const SVector4D &InLhs)
			{
				return InRhs.Distance(InLhs);
			}

			forceinline SVector4D Projection(const SVector4D &InLhs) const;

			forceinline static SVector4D Projection(const SVector4D &InRhs, const SVector4D &InLhs)
			{
				return InRhs.Projection(InLhs);
			}

			forceinline SVector4D Reflect(const SVector4D &InNormal) const;

			forceinline static SVector4D Reflect(const SVector4D &InIncidence, const SVector4D &InNormal)
			{
				return InIncidence.Reflect(InNormal);
			}

			forceinline SVector4D Refract(const SVector4D &InNormal, float InEta) const;

			forceinline static SVector4D Refract(const SVector4D &InIncidence, const SVector4D &InNormal, float InEta)
			{
				return InIncidence.Refract(InNormal, InEta);
			}

			forceinline SVector3D XYZ() const
			{
				return SVector3D(X, Y, Z);
			}
		};

		forceinline bool SVector4D::operator==(const SVector4D &Other) const
		{
			return X == Other.X &&
				Y == Other.Y &&
				Z == Other.Z &&
				W == Other.W;
		}

		forceinline bool SVector4D::operator!=(const SVector4D &Other) const
		{
			return X == Other.X ||
				Y == Other.Y ||
				Z == Other.Z ||
				W == Other.W;
		}

		forceinline bool SVector4D::operator<(const SVector4D &Other) const
		{
			return Length() < Other.Length();
		}

		forceinline bool SVector4D::operator>(const SVector4D &Other) const
		{
			return Length() > Other.Length();
		}

		forceinline bool SVector4D::operator<=(const SVector4D &Other) const
		{
			return Length() <= Other.Length();
		}

		forceinline bool SVector4D::operator>=(const SVector4D &Other) const
		{
			return Length() >= Other.Length();
		}

		forceinline SVector4D operator*(float InScale, const SVector4D &InVector)
		{
			return InVector.operator*(InScale);
		}

		forceinline SVector4D SVector4D::Add(const SVector4D &Other) const
		{
			return SVector4D(X + Other.X, Y + Other.Y, Z + Other.Z, W + Other.W);
		}

		forceinline SVector4D SVector4D::Sub(const SVector4D &Other) const
		{
			return SVector4D(X - Other.X, Y - Other.Y, Z - Other.Z, W - Other.W);
		}

		forceinline SVector4D SVector4D::Mul(const SVector4D &Other) const
		{
			return SVector4D(X * Other.X, Y * Other.Y, Z * Other.Z, W * Other.W);
		}

		forceinline SVector4D SVector4D::Div(const SVector4D &Other) const
		{
			WE_ASSERT(Other.X != 0);
			WE_ASSERT(Other.Y != 0);
			WE_ASSERT(Other.Z != 0);
			WE_ASSERT(Other.W != 0);

			return SVector4D(X / Other.X, Y / Other.Y, Z / Other.Z, W / Other.W);
		}

		forceinline SVector4D SVector4D::Add(float Other) const
		{
			return SVector4D(X + Other, Y + Other, Z + Other, W + Other);
		}

		forceinline SVector4D SVector4D::Sub(float Other) const
		{
			return SVector4D(X - Other, Y - Other, Z - Other, W - Other);
		}

		forceinline SVector4D SVector4D::Mul(float Other) const
		{
			return SVector4D(X * Other, Y * Other, Z * Other, W * Other);
		}

		forceinline SVector4D SVector4D::Div(float Other) const
		{
			WE_ASSERT(Other != 0);

			return SVector4D(X / Other, Y / Other, Z / Other, W / Other);
		}

		forceinline float SVector4D::Dot(const SVector4D &InLhs) const
		{
			return X * InLhs.X + Y * InLhs.Y + Z * InLhs.Z + W * InLhs.W;
		}

		forceinline SVector4D SVector4D::operator+(const SVector4D &InLhs) const
		{
			return SVector4D(X + InLhs.X, Y + InLhs.Y, Z + InLhs.Z, W + InLhs.W);
		}

		forceinline SVector4D SVector4D::operator+(float InScale) const
		{
			return SVector4D(X + InScale, Y + InScale, Z + InScale, W + InScale);
		}

		forceinline SVector4D &SVector4D::operator+=(const SVector4D &InLhs)
		{
			X += InLhs.X;
			Y += InLhs.Y;
			Z += InLhs.Z;
			W += InLhs.W;

			return *this;
		}

		forceinline SVector4D &SVector4D::operator+=(float InScale)
		{
			X += InScale;
			Y += InScale;
			Z += InScale;
			W += InScale;

			return *this;
		}

		forceinline SVector4D SVector4D::operator-(const SVector4D &InLhs) const
		{
			return SVector4D(X - InLhs.X, Y - InLhs.Y, Z - InLhs.Z, W - InLhs.W);
		}

		forceinline SVector4D SVector4D::operator-(float InScale) const
		{
			return SVector4D(X - InScale, Y - InScale, Z - InScale, W - InScale);
		}

		forceinline SVector4D &SVector4D::operator-=(const SVector4D &InLhs)
		{
			X -= InLhs.X;
			Y -= InLhs.Y;
			Z -= InLhs.Z;
			W -= InLhs.W;

			return *this;
		}

		forceinline SVector4D &SVector4D::operator-=(float InScale)
		{
			X -= InScale;
			Y -= InScale;
			Z -= InScale;
			W -= InScale;

			return *this;
		}

		forceinline SVector4D SVector4D::operator*(const SVector4D &InLhs) const
		{
			return SVector4D(X * InLhs.X, Y * InLhs.Y, Z * InLhs.Z, W * InLhs.W);
		}

		forceinline SVector4D SVector4D::operator*(float InScale) const
		{
			return SVector4D(X * InScale, Y * InScale, Z * InScale, W * InScale);
		}

		forceinline SVector4D &SVector4D::operator*=(const SVector4D &InLhs)
		{
			X *= InLhs.X;
			Y *= InLhs.Y;
			Z *= InLhs.Z;
			W *= InLhs.W;

			return *this;
		}

		forceinline SVector4D &SVector4D::operator*=(float InScale)
		{
			X *= InScale;
			Y *= InScale;
			Z *= InScale;
			W *= InScale;

			return *this;
		}

		forceinline SVector4D SVector4D::operator/(const SVector4D &InLhs) const
		{
			WE_ASSERT(InLhs.X != 0);
			WE_ASSERT(InLhs.Y != 0);
			WE_ASSERT(InLhs.Z != 0);
			WE_ASSERT(InLhs.W != 0);

			return SVector4D(X / InLhs.X, Y / InLhs.Y, Z / InLhs.Z, W / InLhs.W);
		}

		forceinline SVector4D SVector4D::operator/(float InScale) const
		{
			WE_ASSERT(InScale != 0);

			return SVector4D(X / InScale, Y / InScale, Z / InScale, W / InScale);
		}

		forceinline SVector4D &SVector4D::operator/=(const SVector4D &InLhs)
		{
			WE_ASSERT(InLhs.X != 0);
			WE_ASSERT(InLhs.Y != 0);
			WE_ASSERT(InLhs.Z != 0);
			WE_ASSERT(InLhs.W != 0);

			X /= InLhs.X;
			Y /= InLhs.Y;
			Z /= InLhs.Z;
			W /= InLhs.W;

			return *this;
		}

		forceinline SVector4D &SVector4D::operator/=(float InScale)
		{
			WE_ASSERT(InScale != 0);

			X /= InScale;
			Y /= InScale;
			Z /= InScale;
			W /= InScale;

			return *this;
		}

		forceinline SVector4D SVector4D::operator-() const
		{
			return SVector4D(-X, -Y, -Z, -W);
		}

		forceinline float &SVector4D::operator[](uint32 InIndex)
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 3);

			if (InIndex == 0)
				return X;
			else if (InIndex == 1)
				return Y;
			else if (InIndex == 2)
				return Z;
			else
				return W;
		}

		forceinline float SVector4D::operator[](uint32 InIndex) const
		{
			WE_ASSERT(InIndex >= 0 && InIndex <= 3);

			if (InIndex == 0)
				return X;
			else if (InIndex == 1)
				return Y;
			else if (InIndex == 2)
				return Z;
			else
				return W;
		}

		forceinline float SVector4D::Length() const
		{
			return Sqrt(Square(X) + Square(Y) + Square(Z) + Square(W));
		}

		forceinline SVector4D SVector4D::Normalized() const
		{
			float Length = this->Length();
			return *this / Length;
		}

		forceinline float SVector4D::Angle(const SVector4D &InLhs) const
		{
			float Dot = this->Dot(InLhs);
			float LengthProduct = this->Length() * InLhs.Length();
			float Cos = Dot / LengthProduct;

			return ACos(Cos);
		}

		forceinline float SVector4D::Distance(const SVector4D &InLhs) const
		{
			return (InLhs - *this).Length();
		}

		forceinline SVector4D SVector4D::Projection(const SVector4D &InLhs) const
		{
			float Dot = this->Dot(InLhs);
			float Length = this->Length();

			return this->Normalized() * (Dot / Length);
		}

		forceinline SVector4D SVector4D::Reflect(const SVector4D &InNormal) const
		{
			return *this - 2.0f * InNormal * InNormal.Dot(*this);
		}

		forceinline SVector4D SVector4D::Refract(const SVector4D &InNormal, float InEta) const
		{
			float CosI = -(*this).Dot(InNormal);
			float Cost2 = 1.0f - Sqrt(InEta) * (1.0f - Square(CosI));
			SVector4D T = InEta * (*this) + ((InEta * CosI - Sqrt(Abs(Cost2))) * InNormal);
			return T * (SVector4D)(Cost2 > 0);
		}

		forceinline static SVector4D Max(const SVector4D &InA, const SVector4D &InB)
		{
			return InA > InB ? InA : InB;
		}

		forceinline static SVector4D Min(const SVector4D &InA, const SVector4D &InB)
		{
			return InA < InB ? InA : InB;
		}

		forceinline static SVector4D Clamp(const SVector4D &InMin, const SVector4D &InMax, const SVector4D &InValue)
		{
			return Max(InMin, Min(InMax, InValue));
		}

		forceinline static SVector4D Saturate(const SVector4D &InValue)
		{
			return Clamp(SVector4D::Zero, SVector4D::One, InValue);
		}

		forceinline static SVector4D Lerp(const SVector4D &InA, const SVector4D &InB, float InDelta)
		{
			return SVector4D(
				((1.0f - InDelta) * InA.X + InB.X * InDelta),
				((1.0f - InDelta) * InA.Y + InB.Y * InDelta),
				((1.0f - InDelta) * InA.Z + InB.Z * InDelta),
				((1.0f - InDelta) * InA.W + InB.W * InDelta));
		}

	} // namespace Math
} // namespace WindmillEngine