// <Eremita Copyrights>

#pragma once

#include "Math.h"
#include <fstream>
#include <vector>
#include <cstring>

#define MOLLER_TRUMBORE 1
#define TEXCOORD_SIZE 3

namespace WindmillEngine
{
	namespace Math
	{
		struct SVertex
		{
			SVector3D Position = SVector3D::Zero;
			SColor32 Color = { 0, 0, 0, 1 };
			SVector3D Normal = SVector3D::Zero;
			SVector3D Tangent = SVector3D::Zero;
			SVector2D TexCoord[TEXCOORD_SIZE] = { SVector2D::Zero };

			SVertex() noexcept
				: Color(1.0f, 1.0f, 1.0f), Normal(SVector3D::Up), Tangent(SVector3D::Right)
			{
			}

			SVertex(const SVector3D &InPosition)
				: Position(InPosition), Color(1.0f, 1.0f, 1.0f), Normal(SVector3D::Up), Tangent(SVector3D::Right)
			{
			}

			/*friend SArchive &operator<<(SArchive &InArquive, SVertex &InVertex)
			{
				return InArquive
					<< InVertex.Position
					<< InVertex.Color
					<< InVertex.Normal
					<< InVertex.Tangent
					<< InVertex.TexCoord[0]
					<< InVertex.TexCoord[1]
					<< InVertex.TexCoord[2];
			}

			friend SArchive &operator>>(SArchive &InArquive, SVertex &InVertex)
			{
				return InArquive
					>> InVertex.Position
					>> InVertex.Color
					>> InVertex.Normal
					>> InVertex.Tangent
					>> InVertex.TexCoord[0]
					>> InVertex.TexCoord[1]
					>> InVertex.TexCoord[2];
			}

			bool Serialize(SArchive &aArquive)
			{
				aArquive << *this;
				return true;
			}

			bool Deserialize(SArchive &aArquive)
			{
				aArquive
					>> Position
					>> Color
					>> Normal
					>> Tangent
					>> TexCoord[0]
					>> TexCoord[1]
					>> TexCoord[2];

				return true;
			}*/
		};

		struct STriangle
		{

			SVertex V0;
			SVertex V1;
			SVertex V2;

			STriangle() noexcept
			{
			}

			STriangle(const SVector3D &InV0, const SVector3D &InV1, const SVector3D &InV2)
				: V0(InV0), V1(InV1), V2(InV2)
			{
			}

			STriangle(const SVertex &InV0, const SVertex &InV1, const SVertex &InV2)
				: V0(InV0), V1(InV1), V2(InV2)
			{
			}

			bool Intersect(const SRay &InRay, float Barycentric[3], float &InOutDepth)
			{
#ifdef MOLLER_TRUMBORE
				SVector3D v0v1 = V1.Position - V0.Position;
				SVector3D v0v2 = V2.Position - V0.Position;
				SVector3D PVec = InRay.Direction.Cross(v0v2);
				float Det = v0v1.Dot(PVec);

				if (Abs(Det) < EPSILON)
					return false;

				float InvDet = 1.0f / Det;

				SVector3D TVec = InRay.Origin - V0.Position;
				Barycentric[0] = TVec.Dot(PVec) * InvDet;
				if (Barycentric[0] < 0.f || Barycentric[0] > 1.f)
					return false;

				SVector3D QVec = TVec.Cross(v0v1);
				Barycentric[1] = InRay.Direction.Dot(QVec) * InvDet;
				if (Barycentric[1] < 0.f || Barycentric[1] > 1.f)
					return false;

				InOutDepth = v0v2.Dot(QVec) * InvDet;
#endif

				return true;
			}

			forceinline SVertex& operator[](uint32 Index)
			{
				if (Index == 0)
					return V0;
				else if (Index == 1)
					return V1;
				else
					return V2;
			}

			/*friend SArchive &operator<<(SArchive &InArquive, STriangle &InTriangle)
			{
				return InArquive << InTriangle.V0 << InTriangle.V1 << InTriangle.V2;
			}

			friend SArchive &operator>>(SArchive &InArquive, STriangle &InTriangle)
			{
				return InArquive >> InTriangle.V0 >> InTriangle.V1 >> InTriangle.V2;
			}

			bool Serialize(SArchive &aArquive)
			{
				aArquive << *this;
				return true;
			}

			bool Deserialize(SArchive &aArquive)
			{
				aArquive >> V0 >> V1 >> V2;
				return true;
			}*/
		};
	} // namespace Math
} // namespace WindmillEngine