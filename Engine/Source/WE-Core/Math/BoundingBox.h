// <Eremita Copyrights>

#pragma once

#include "Math.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct SBBox
		{
			SVector3D Min;

			SVector3D Max;

			forceinline SBBox() noexcept
				: Min(SVector3D::Zero)
				, Max(SVector3D::One)
			{}

			forceinline SBBox(const SVector3D InMin, const SVector3D InMax)
				: Min(InMin)
				, Max(InMax)
			{}

			forceinline SVector3D GetCenter() const
			{
				return (Min + Max) * 0.5f;
			}

			forceinline SVector3D GetExtent() const
			{
				return (Max - Min) * 0.5f;
			}

			forceinline SVector3D GetSize() const
			{
				return (Max - Min);
			}

			SBBox GetTransformed(const Math::SMatrix4x4& InTransformMatrix)
			{
				return SBBox((Min * InTransformMatrix).XYZ(), (Max * InTransformMatrix).XYZ());
			}

			void SetTransformMatrix(const Math::SMatrix4x4& InTransformMatrix)
			{
				Max = (Max * InTransformMatrix).XYZ();
				Min = (Min * InTransformMatrix).XYZ();
			}

			SBBox(const std::vector<SVector3D>& InPoints)
			{
				Min = SVector3D(INFINITY);
				Max = -SVector3D(INFINITY);

				for (SVector3D V : InPoints)
				{
					if (V < Min)
						Min = V;
					if (V > Max)
						Max = V;
				}
			}

			SBBox(const std::vector<SVertex>& InVertex)
			{
				Min = SVector3D(INFINITY);
				Max = -SVector3D(INFINITY);

				for (SVertex V : InVertex)
				{
					Min.X = Math::Min(Min.X, V.Position.X);
					Min.Y = Math::Min(Min.Y, V.Position.Y);
					Min.Z = Math::Min(Min.Z, V.Position.Z);

					Max.X = Math::Max(Max.X, V.Position.X);
					Max.Y = Math::Max(Max.Y, V.Position.Y);
					Max.Z = Math::Max(Max.Z, V.Position.Z);
				}
			}
		};
	} // namespace Math
} // namespace WindmillEngine