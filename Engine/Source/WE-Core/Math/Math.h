// <Eremita Copyrights>

#pragma once

#include "WECore.h"

constexpr float PI = 3.14159265359f;
#define EPSILON 1.0e-8f
#define MAX_FLOAT 3.0e+23f

namespace WindmillEngine
{
	namespace Math
	{
		static float Sin(float InX) { return sinf(InX); }
		static float ASin(float InX) { return asinf(InX); }
		static float SinH(float InX) { return sinhf(InX); }
		static float Cos(float InX) { return cosf(InX); }
		static float ACos(float InX) { return acosf(InX); }
		static float CosH(float InX) { return coshf(InX); }
		static float Tan(float InX) { return tanf(InX); }
		static float ATan(float InX) { return atanf(InX); }
		static float Sqrt(float InX) { return sqrtf(InX); }
		static float Pow(float InX, float InP) { return powf(InX, InP); }

		static float Abs(float InX) { return fabs(InX); }

		forceinline static float DegToRad(float InAngle)
		{
			return (PI * InAngle) / 180.0f;
		}

		forceinline static float RadToDeg(float InAngle)
		{
			return 180.0f / PI * InAngle;
		}

		forceinline static float Max(float InA, float InB)
		{
			return InA > InB ? InA : InB;
		}

		forceinline static float Min(float InA, float InB)
		{
			return InA < InB ? InA : InB;
		}

		forceinline static float Clamp(float InMin, float InMax, float InValue)
		{
			return Max(InMin, Min(InMax, InValue));
		}

		forceinline static float Saturate(float InValue)
		{
			return Clamp(0.0f, 1.0f, InValue);
		}

		forceinline static float Lerp(float InA, float InB, float InDelta)
		{
			return (1.0f - InDelta) * InA + InB * InDelta;
		}

		forceinline static float Square(float InX)
		{
			return InX * InX;
		}

		forceinline static bool IsPowerOfTwo(int32 InValue)
		{
			return ((InValue != 0) && ((InValue & (~InValue + 1)) == InValue));
		}

		forceinline static bool SolveQuadradic(float InA, float InB, float InC, float *OutX0, float *OutX1)
		{
			float Delta = Square(InB) - 4.0f * InA * InC;

			if (Delta > 0.0f)
			{
				*OutX0 = (-InB + Sqrt(Delta)) / 2.0f * InA;
				*OutX1 = (-InB - Sqrt(Delta)) / 2.0f * InA;
			}
			else if (Delta == 0.0f)
			{
				*OutX0 = (-InB + Sqrt(Delta)) / 2.0f * InA;
				*OutX1 = *OutX0;
			}
			else
			{
				return false;
			}

			if (OutX0 > OutX1)
				std::swap(OutX0, OutX1);

			return true;
		}
	} // namespace Math
} // namespace WindmillEngine