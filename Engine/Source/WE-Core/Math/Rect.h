// <Eremita Copyrights>

#pragma once

#include "Math.h"
#include "Vector2D.h"
#include "Vector3D.h"

namespace WindmillEngine
{
	namespace Math
	{
		struct CORE_API SRect
		{
			float X = 0.0f;
			float Y = 0.0f;

			float Width = 0.0f;
			float Height = 0.0f;

			forceinline SRect() noexcept
			{
			}

			forceinline SRect(float InX, float InY, float InWidth, float InHeight);

			forceinline SRect(SVector2D InPosition, SVector2D InSize);

			forceinline SVector2D GetPosition() const
			{
				return SVector2D(X, Y);
			}

			forceinline SVector2D GetSize() const
			{
				return SVector2D(Width, Height);
			}

			forceinline SVector2D GetCenter() const
			{
				return SVector2D(X, Y) * 0.5f;
			}

			forceinline SVector2D GetMin() const
			{
				return GetPosition();
			}

			forceinline SVector2D GetMax() const
			{
				return GetMin() + GetSize();
			}

			forceinline bool Contains(const SVector2D& InPoint) const;

			forceinline bool Contains(const SVector3D& InPoint) const;

			forceinline bool operator==(const SRect& Other) const
			{
				return X == Other.X && Y == Other.Y
					&& Width == Other.Width && Height == Other.Height;
			}
		};

		SRect::SRect(float InX, float InY, float InWidth, float InHeight)
			:X(InX), Y(InY), Width(InWidth), Height(InHeight)
		{
		}

		SRect::SRect(SVector2D InPosition, SVector2D InSize)
			: X(InPosition.X), Y(InPosition.Y), Width(InSize.X), Height(InSize.Y)
		{
		}

		bool SRect::Contains(const SVector2D &InPoint) const
		{
			return InPoint.X >= GetMin().X && InPoint.X <= GetMax().X
				&& InPoint.Y >= GetMin().Y && InPoint.Y <= GetMax().Y;
		}

		bool SRect::Contains(const SVector3D& InPoint) const
		{
			return Contains(SVector2D(InPoint.X, InPoint.Y));
		}
	} // namespace Math
} // namespace WindmillEngine