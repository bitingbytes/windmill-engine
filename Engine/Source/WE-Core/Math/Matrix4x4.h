// <Eremita Copyrights>

#pragma once

#include "Math.h"
#include "Vector4D.h"
#include <cstring>

namespace WindmillEngine
{
	namespace Math
	{
		/*
		*   SMatrix
		*   Row Major
		*   z-axis
		*     ^
		*     |
		*     |  y-axis
		*     |  /
		*     | /
		*     |/
		*     +--------------->   x-axis
		*/
		struct CORE_API SMatrix4x4
		{
			union {
				ALIGN(16) float E[4][4] = { { 0 } };
				ALIGN(16) float D[16];
				ALIGN(16) SVector4D Row[4];
			};

			forceinline SMatrix4x4() noexcept
			{
			}

			forceinline SMatrix4x4(float InDi);

			forceinline SMatrix4x4& operator=(const SMatrix4x4& InOther);

			forceinline SMatrix4x4(const SVector4D& InRow0, const SVector4D& InRow1, const SVector4D& InRow2, const SVector4D& InRow3);

			forceinline SMatrix4x4(const SVector3D& InRow0, const SVector3D& InRow1, const SVector3D& InRow2, const SVector3D& InRow3 = SVector3D::One);

			forceinline SMatrix4x4 operator*(const SMatrix4x4& Other) const;

			forceinline void operator*=(const SMatrix4x4& Other);

			forceinline SMatrix4x4 operator*(float InScalar) const;

			forceinline SMatrix4x4 operator+(const SMatrix4x4& Other) const;

			forceinline SMatrix4x4& operator+=(const SMatrix4x4& Other);

			forceinline SMatrix4x4 operator+(float InScalar) const;

			forceinline SMatrix4x4 Transposed() const;

			forceinline static SMatrix4x4 Translate(const SVector3D& InTranslation)
			{
				SMatrix4x4 Temp = SMatrix4x4(1.0f);
				Temp.E[3][0] = InTranslation.X; Temp.E[3][1] = InTranslation.Y; Temp.E[3][2] = InTranslation.Z;

				return Temp;
			}

			forceinline static SMatrix4x4 TRS(const SVector3D& InPosition, const SVector3D& InRotation, const SVector3D& InScale)
			{
				return Translate(InPosition) * Rotate(InRotation.X, InRotation.Y, InRotation.Z) * Scale(InScale);
			}

			forceinline static SMatrix4x4 Scale(const SVector3D& InScale)
			{
				SMatrix4x4 Temp = SMatrix4x4(1.0f);
				Temp.E[0][0] = InScale.X; Temp.E[1][1] = InScale.Y; Temp.E[2][2] = InScale.Z;

				return Temp;
			}

			forceinline static SMatrix4x4 Rotate(const SVector3D& InRotation)
			{
				return Rotate(InRotation.X, InRotation.Y, InRotation.Z);
			}

			forceinline static SMatrix4x4 Rotate(float InPitch, float InRoll, float InYaw)
			{
				return RotateY(InRoll) * RotateX(InPitch) * RotateZ(InYaw);
			}

			forceinline static SMatrix4x4 RotateX(float InAngle)
			{
				float Angle = Math::DegToRad(InAngle);
				float CosAngle = Math::Cos(Angle);
				float SinAngle = Math::Sin(Angle);

				return SMatrix4x4(
					SVector4D(1.0f, 0.0f, 0.0f, 0.0f),
					SVector4D(0.0f, CosAngle, -SinAngle, 0.0f),
					SVector4D(0.0f, SinAngle, CosAngle, 0.0f),
					SVector4D(0.0f, 0.0f, 0.0f, 1.0f)
				);
			}

			forceinline static SMatrix4x4 RotateZ(float InAngle)
			{
				float Angle = Math::DegToRad(InAngle);

				float CosAngle = Math::Cos(Angle);
				float SinAngle = Math::Sin(Angle);

				return SMatrix4x4(
					SVector4D(CosAngle, 0.0f, SinAngle, 0.0f),
					SVector4D(0.0f, 1.0f, 0.0f, 0.0f),
					SVector4D(-SinAngle, 0.0f, CosAngle, 0.0f),
					SVector4D(0.0f, 0.0f, 0.0f, 1.0f)
				);
			}

			forceinline static SMatrix4x4 RotateY(float InAngle)
			{
				float Angle = Math::DegToRad(InAngle);

				float CosAngle = Math::Cos(Angle);
				float SinAngle = Math::Sin(Angle);

				return SMatrix4x4(
					SVector4D(CosAngle, -SinAngle, 0.0f, 0.0f),
					SVector4D(SinAngle, CosAngle, 0.0f, 0.0f),
					SVector4D(0.0f, 0.0f, 1.0f, 0.0f),
					SVector4D(0.0f, 0.0f, 0.0f, 1.0f)
				);
			}

			// TODO: Implement Orthogonal Projection Matrix
			forceinline static SMatrix4x4 Orthogonal()
			{
				return SMatrix4x4(1.0f);
			}

			forceinline static SMatrix4x4 Perspective(float InNear, float InFar, float InAspect, float InFov)
			{

				// FPlane(1.0f / FMath::Tan(HalfFOV), 0.0f, 0.0f, 0.0f),
				// FPlane(0.0f, Width / FMath::Tan(HalfFOV) / Height, 0.0f, 0.0f),
				// FPlane(0.0f, 0.0f, ((MinZ == MaxZ) ? (1.0f - Z_PRECISION) : MaxZ / (MaxZ - MinZ)), 1.0f),
				// FPlane(0.0f, 0.0f, -MinZ * ((MinZ == MaxZ) ? (1.0f - Z_PRECISION) : MaxZ / (MaxZ - MinZ)), 0.0f)
				float HalfFov = Math::DegToRad(InFov) * 0.5f;
				return SMatrix4x4(
					SVector4D(1.0f / Math::Tan(HalfFov), 0.0f, 0.0f, 0.0),
					SVector4D(0.0f, 0.0f, InFar / (InFar - InNear), 1.0f),
					SVector4D(0.0f, InAspect / Math::Tan(HalfFov), 0.0f, 0.0f),
					SVector4D(0.0f, 0.0f, -InNear * InFar / (InFar - InNear), 0.0f)
				);
			}

			forceinline static SMatrix4x4 LookAt(const SVector3D& InEyePosition, const SVector3D& InFocusPoint, const SVector3D& InUpDirection = SVector3D::Up)
			{
				const SVector3D ForwardVector = (InFocusPoint - InEyePosition).Normalized(); // Y
				const SVector3D RightVector = SVector3D::Cross(ForwardVector, InUpDirection).Normalized(); // X
				const SVector3D UpVector = SVector3D::Cross(RightVector, ForwardVector); // Z

				SMatrix4x4 M(1.0f);

				for (uint32 RowIndex = 0; RowIndex < 3; RowIndex++)
				{
					M.E[RowIndex][0] = (&RightVector.X)[RowIndex];		// X
					M.E[RowIndex][1] = (&UpVector.X)[RowIndex];			// Z
					M.E[RowIndex][2] = (&ForwardVector.X)[RowIndex];	// Y
					M.E[RowIndex][3] = 0.0f;
				}
				M.E[3][0] = -SVector3D::Dot(InEyePosition, RightVector);   	 // X
				M.E[3][1] = -SVector3D::Dot(InEyePosition, UpVector);		 // Z
				M.E[3][2] = -SVector3D::Dot(InEyePosition, ForwardVector);	 // Y
				M.E[3][3] = 1.0f;

				return M;
			}

			/*
			forceinline static SMatrix4x4 LookAt(const SVector3D& InEyePosition, const SVector3D& InFocusPoint, const SVector3D& InUpDirection = SVector3D::Up)
			{
				SVector3D Forward = (InFocusPoint - InEyePosition).Normalized();
				SVector3D Right = SVector3D::Cross(Forward, InUpDirection.Normalized());
				SVector3D Up = SVector3D::Cross(Right, Forward).Normalized();

				SMatrix4x4 Temp(1.0f);

				Temp.E[0][0] = Right.X;
				Temp.E[0][1] = Right.Y;
				Temp.E[0][2] = Right.Z;

				Temp.E[1][0] = Up.X;
				Temp.E[1][1] = Up.Y;
				Temp.E[1][2] = Up.Z;

				Temp.E[2][0] = Forward.X;
				Temp.E[2][1] = Forward.Y;
				Temp.E[2][2] = Forward.Z;

				return Temp * Translate(-InEyePosition);
			}
			*/
		};

		forceinline SMatrix4x4::SMatrix4x4(float InDi)
		{
			E[0][0] = InDi; E[0][1] = 0.0f; E[0][2] = 0.0f; E[0][3] = 0.0f;
			E[1][0] = 0.0f; E[1][1] = InDi; E[1][2] = 0.0f; E[1][3] = 0.0f;
			E[2][0] = 0.0f; E[2][1] = 0.0f; E[2][2] = InDi; E[2][3] = 0.0f;
			E[3][0] = 0.0f; E[3][1] = 0.0f; E[3][2] = 0.0f; E[3][3] = InDi;

		}

		forceinline SMatrix4x4& SMatrix4x4::operator=(const SMatrix4x4 & InOther)
		{
			Row[0] = InOther.Row[0];
			Row[1] = InOther.Row[1];
			Row[2] = InOther.Row[2];
			Row[3] = InOther.Row[3];

			return *this;
		}

		forceinline SMatrix4x4::SMatrix4x4(const SVector4D& InRow0, const SVector4D& InRow1, const SVector4D& InRow2, const SVector4D& InRow3)
		{
			E[0][0] = InRow0.X; E[0][1] = InRow0.Y; E[0][2] = InRow0.Z; E[0][3] = InRow0.W;
			E[1][0] = InRow1.X; E[1][1] = InRow1.Y; E[1][2] = InRow1.Z; E[1][3] = InRow1.W;
			E[2][0] = InRow2.X; E[2][1] = InRow2.Y; E[2][2] = InRow2.Z; E[2][3] = InRow2.W;
			E[3][0] = InRow3.X; E[3][1] = InRow3.Y; E[3][2] = InRow3.Z; E[3][3] = InRow3.W;
		}

		forceinline SMatrix4x4::SMatrix4x4(const SVector3D& InRow0, const SVector3D& InRow1, const SVector3D& InRow2, const SVector3D& InRow3)
		{
			E[0][0] = InRow0.X; E[0][1] = InRow0.Y; E[0][2] = InRow0.Z; E[0][3] = 1.0f;
			E[1][0] = InRow1.X; E[1][1] = InRow1.Y; E[1][2] = InRow1.Z; E[1][3] = 1.0f;
			E[2][0] = InRow2.X; E[2][1] = InRow2.Y; E[2][2] = InRow2.Z; E[2][3] = 1.0f;
			E[3][0] = InRow3.X; E[3][1] = InRow3.Y; E[3][2] = InRow3.Z; E[3][3] = 1.0f;
		}

		forceinline static void MatrixMultiplicator(SMatrix4x4* OutDestination, const SMatrix4x4* InSource, const SMatrix4x4* InOther)
		{
			SMatrix4x4 Temp = SMatrix4x4(0.0f);

			SVector4D Row0 = SVector4D(InSource->E[0][0], InSource->E[0][1], InSource->E[0][2], InSource->E[0][3]);
			SVector4D Row1 = SVector4D(InSource->E[1][0], InSource->E[1][1], InSource->E[1][2], InSource->E[1][3]);
			SVector4D Row2 = SVector4D(InSource->E[2][0], InSource->E[2][1], InSource->E[2][2], InSource->E[2][3]);
			SVector4D Row3 = SVector4D(InSource->E[3][0], InSource->E[3][1], InSource->E[3][2], InSource->E[3][3]);

			SVector4D Col0 = SVector4D(InOther->E[0][0], InOther->E[1][0], InOther->E[2][0], InOther->E[3][0]);
			SVector4D Col1 = SVector4D(InOther->E[0][1], InOther->E[1][1], InOther->E[2][1], InOther->E[3][1]);
			SVector4D Col2 = SVector4D(InOther->E[0][2], InOther->E[1][2], InOther->E[2][2], InOther->E[3][2]);
			SVector4D Col3 = SVector4D(InOther->E[0][3], InOther->E[1][3], InOther->E[2][3], InOther->E[3][3]);

			Temp.E[0][0] = Row0.Dot(Col0); Temp.E[0][1] = Row0.Dot(Col1); Temp.E[0][2] = Row0.Dot(Col2); Temp.E[0][3] = Row0.Dot(Col3);
			Temp.E[1][0] = Row1.Dot(Col0); Temp.E[1][1] = Row1.Dot(Col1); Temp.E[1][2] = Row1.Dot(Col2); Temp.E[1][3] = Row1.Dot(Col3);
			Temp.E[2][0] = Row2.Dot(Col0); Temp.E[2][1] = Row2.Dot(Col1); Temp.E[2][2] = Row2.Dot(Col2); Temp.E[2][3] = Row2.Dot(Col3);
			Temp.E[3][0] = Row3.Dot(Col0); Temp.E[3][1] = Row3.Dot(Col1); Temp.E[3][2] = Row3.Dot(Col2); Temp.E[3][3] = Row3.Dot(Col3);

			memcpy(OutDestination, &Temp, 16 * sizeof(float));
		}

		forceinline SMatrix4x4 SMatrix4x4::operator*(const SMatrix4x4& Other) const
		{
			SMatrix4x4 Temp = SMatrix4x4(0.0f);

			MatrixMultiplicator(&Temp, this, &Other);

			return Temp;
		}

		forceinline void SMatrix4x4::operator*=(const SMatrix4x4& Other)
		{
			MatrixMultiplicator(this, this, &Other);
		}

		forceinline SMatrix4x4 SMatrix4x4::operator*(float InScalar) const
		{
			SMatrix4x4 Temp = *this;
			for (uint32 i = 0; i < 4; ++i)
			{
				for (uint32 j = 0; j < 4; ++j)
				{
					Temp.E[i][j] *= InScalar;
				}
			}

			return Temp;
		}

		forceinline SMatrix4x4 SMatrix4x4::operator+(const SMatrix4x4& Other) const
		{
			SMatrix4x4 Temp = *this;
			for (uint32 i = 0; i < 4; ++i)
			{
				for (uint32 j = 0; j < 4; ++j)
				{
					Temp.E[i][j] += Other.E[i][j];
				}
			}

			return Temp;
		}

		forceinline SMatrix4x4& SMatrix4x4::operator+=(const SMatrix4x4& Other)
		{
			for (uint32 i = 0; i < 4; ++i)
			{
				for (uint32 j = 0; j < 4; ++j)
				{
					this->E[i][j] += Other.E[i][j];
				}
			}

			return *this;
		}

		forceinline SMatrix4x4 SMatrix4x4::operator+(float InScalar) const
		{
			SMatrix4x4 Temp = *this;
			for (uint32 i = 0; i < 4; ++i)
			{
				for (uint32 j = 0; j < 4; ++j)
				{
					Temp.E[i][j] += InScalar;
				}
			}

			return Temp;
		}

		forceinline SVector4D operator*(const SVector4D& InVector, const SMatrix4x4& InMatrix)
		{
			SVector4D Col0 = SVector4D(InMatrix.E[0][0], InMatrix.E[1][0], InMatrix.E[2][0], InMatrix.E[3][0]);
			SVector4D Col1 = SVector4D(InMatrix.E[0][1], InMatrix.E[1][1], InMatrix.E[2][1], InMatrix.E[3][1]);
			SVector4D Col2 = SVector4D(InMatrix.E[0][2], InMatrix.E[1][2], InMatrix.E[2][2], InMatrix.E[3][2]);
			SVector4D Col3 = SVector4D(InMatrix.E[0][3], InMatrix.E[1][3], InMatrix.E[2][3], InMatrix.E[3][3]);

			float TempX = InVector.Dot(Col0);
			float TempY = InVector.Dot(Col1);
			float TempZ = InVector.Dot(Col2);
			float TempW = InVector.Dot(Col3);

			return SVector4D(TempX, TempY, TempZ, TempW);
		}

		forceinline SMatrix4x4 SMatrix4x4::Transposed() const
		{
			SMatrix4x4 Temp;

			for (uint32 j = 0; j < 4; j++)
			{
				for (uint32 i = 0; i < 4; ++i)
				{
					Temp.E[i][j] = E[j][i];
				}
			}

			return Temp;
		}

	} // namespace Math
} // namespace WindmillEngine