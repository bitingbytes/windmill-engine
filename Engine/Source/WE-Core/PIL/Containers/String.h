// <Eremita Copyrights>

#pragma once

#include "System.h"
#include "WECoreLIB.h"

#ifdef _UNICODE
typedef std::wstring CORE_API SString;
#else
typedef std::string CORE_API SString;
#endif

class CORE_API String
{
public:
	inline static SString GetFirstToken(const SString &InLine)
	{
		if (!InLine.empty())
		{
			const size_t token_start = InLine.find_first_not_of(TEXT(" \t"));
			const size_t token_end = InLine.find_first_of(TEXT(" \t"), token_start);
			if (token_start != SString::npos && token_end != SString::npos)
			{
				return InLine.substr(token_start, token_end - token_start);
			}
			else if (token_start != SString::npos)
			{
				return InLine.substr(token_start);
			}
		}
		return TEXT("");
	}

	inline static SString Tail(const SString &InLine)
	{
		const size_t token_start = InLine.find_first_not_of(TEXT(" \t"));
		const size_t space_start = InLine.find_first_of(TEXT(" \t"), token_start);
		const size_t tail_start = InLine.find_first_not_of(TEXT(" \t"), space_start);
		const size_t tail_end = InLine.find_last_not_of(TEXT(" \t"));
		if (tail_start != SString::npos && tail_end != SString::npos)
		{
			return InLine.substr(tail_start, tail_end - tail_start + 1);
		}
		else if (tail_start != SString::npos)
		{
			return InLine.substr(tail_start);
		}
		return TEXT("");
	}

	inline static void Split(const SString &InLine, std::vector<SString> &OutSplitted, SString InToken)
	{
		OutSplitted.clear();

		SString temp;

		for (int i = 0; i < (int)InLine.size(); i++)
		{
			SString test = InLine.substr(i, InToken.size());

			if (test == InToken)
			{
				if (!temp.empty())
				{
					OutSplitted.push_back(temp);
					temp.clear();
					i += (int)InToken.size() - 1;
				}
				else
				{
					OutSplitted.push_back(TEXT(""));
				}
			}
			else if (i + InToken.size() >= InLine.size())
			{
				temp += InLine.substr(i, InToken.size());
				OutSplitted.push_back(temp);
				break;
			}
			else
			{
				temp += InLine[i];
			}
		}
	}
};