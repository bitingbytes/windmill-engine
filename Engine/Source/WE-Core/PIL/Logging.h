// <Eremita Copyrights>

#pragma once

#include "System.h"
#include "WECoreLIB.h"

namespace WindmillEngine
{
	static std::mutex mu;

	enum ELogLevel : uint8
	{
		LL_Fatal = 0,
		LL_Error,
		LL_Warning,
		LL_Info,
		LL_Verbose
	};

	class CORE_API Logger
	{
	public:
		static const char* LogLevelToString(ELogLevel InLogLevel)
		{
			switch (InLogLevel)
			{
			case LL_Fatal:
				return "FAT";
			case LL_Error:
				return "ERR";
			case LL_Warning:
				return "WAR";
			case LL_Info:
				return "INF";
			case LL_Verbose:
				return "VER";
			}
			return "";
		}

		//static void SetLogLevel(ELogLevel _Level)
		//{
		//	sLogLevel = _Level;
		//}

		static void PlatformLogMessage(ELogLevel InLevel);

		static void PlatformClearLogMessage();

		static void LogInternal(const char* InCategory, ELogLevel InLogLevel, const char* InFormat, ...)
		{
			//if (InLogLevel >= sLogLevel)
			//{
			std::lock_guard<std::mutex> Lock(mu);
			PlatformLogMessage(InLogLevel);

			std::thread::id Id = std::this_thread::get_id();

			std::cout << "[" << Id << "]";
			va_list ap;
			printf("[%s][%s] ", InCategory, LogLevelToString(InLogLevel));
			va_start(ap, InFormat);
			vprintf(InFormat, ap);
			va_end(ap);
			printf("%c", '\n');

			PlatformClearLogMessage();
			//}
		}

		//private:
		//	static uint8 sLogLevel;
	};

	//uint8 Logger::sLogLevel = LL_Info;
} // namespace WindmillEngine

#if 1
#define WE_LOG(Level, Msg, ...) WindmillEngine::Logger::LogInternal(__TIME__, Level, Msg, ##__VA_ARGS__)
#define WE_FATAL(Msg, ...) WindmillEngine::Logger::LogInternal(__TIME__, LL_Fatal, Msg, ##__VA_ARGS__)
#define WE_ERROR(Msg, ...) WindmillEngine::Logger::LogInternal(__TIME__, LL_Error, Msg, ##__VA_ARGS__)
#define WE_WARN(Msg, ...) WindmillEngine::Logger::LogInternal(__TIME__, LL_Warning, Msg, ##__VA_ARGS__)
#define WE_INFO(Msg, ...) WindmillEngine::Logger::LogInternal(__TIME__, LL_Info, Msg, ##__VA_ARGS__)
#define WE_VERB(Msg, ...) WindmillEngine::Logger::LogInternal(__TIME__, LL_Verbose, Msg, ##__VA_ARGS__)
#else
#define WE_LOG(Level, Msg, ...) 
#define WE_FATAL(Msg, ...) 
#define WE_ERROR(Msg, ...) 
#define WE_WARN(Msg, ...) 
#define WE_INFO(Msg, ...) 
#define WE_VERB(Msg, ...) 
#endif