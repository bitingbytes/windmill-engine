// <Eremita Copyrights>

#pragma once

namespace WindmillEngine
{
	// Unsigned int of 8 bits
	typedef unsigned char uint8;

	// Unsigned int of 16 bits
	typedef unsigned short int uint16;

	// Unsigned int of 32 bits
	typedef unsigned int uint32;

	// Unsigned int of 64 bits
	typedef unsigned long long uint64;

	// Signed int of 8 bits
	typedef signed char int8;

	// Signed int of 16 bits
	typedef signed short int int16;

	// Signed int of 32 bits
	typedef signed int int32;

	// Signed int of 64 bits
	typedef signed long long int64;

	// ANSI character.
	typedef char ansichar;

	// Wide character
	typedef wchar_t wchar;

	// 8 bits
	typedef unsigned char byte;
} // namespace WindmillEngine
