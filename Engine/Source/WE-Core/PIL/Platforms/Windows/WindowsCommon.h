// <Eremita Copyrights>

#pragma once

#if WE_DEBUG
#define forceinline inline
#define DEBUG_BREAK() __debugbreak()
#else
#define forceinline __forceinline
#define DEBUG_BREAK() 
#endif

#if defined(WE_EXPORT_DLL)
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLIMPORT __declspec(dllimport)
#endif

#define ALIGN(n) __declspec(align(n)) 

#include <windows.h>

#ifndef TEXT
#define TEXT(x) L##x
#endif