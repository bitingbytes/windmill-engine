#include "PIL/Application/Application.h"

namespace WindmillEngine
{
	static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

	struct SMessage
	{
		HWND hWnd = 0;
		UINT UMessage = 0;
		WPARAM WParam = 0;
		LPARAM LParam = 0;
	};

	void Application::Initialize()
	{
		WNDCLASS WindowClass;
		ZeroMemory(&WindowClass, sizeof(WindowClass));
		WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
		WindowClass.lpfnWndProc = WndProc;
		WindowClass.cbClsExtra = 0;
		WindowClass.cbWndExtra = 0;
		WindowClass.hInstance = GetModuleHandle(0);
		WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		WindowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		WindowClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
		WindowClass.lpszMenuName = NULL;
		WindowClass.lpszClassName = TEXT("WindmillWindow");

		if (!::RegisterClass(&WindowClass))
		{
			WE_ERROR("Could not register class. (%d)", GetLastError());
			return;
		}
		if (sApplicationHandler == nullptr)
			sApplicationHandler = this;
	}

	void* Application::ProcessMessages(SMessage&& InMessage)
	{
		switch (InMessage.UMessage)
		{
			HDC         hdc;
			PAINTSTRUCT ps;
		case WM_PAINT:
			hdc = BeginPaint(InMessage.hWnd, &ps);

			EndPaint(InMessage.hWnd, &ps);
			return 0;
			// Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			m_State = EApplicationState::AS_Exiting;
			return 0;
		}

		// Check if a key has been pressed on the keyboard.
		case WM_KEYDOWN:
		{
			// If a key is pressed send it to the input object so it can record that state.
			// m_Input->KeyDown((unsigned int)wparam);
			return 0;
		}

		// Check if a key has been released on the keyboard.
		case WM_KEYUP:
		{
			// If a key is released then send it to the input object so it can unset the state for that key.
			// m_Input->KeyUp((unsigned int)wparam);
			return 0;
		}
		default:
		{
			return (LRESULT*)DefWindowProc(InMessage.hWnd, InMessage.UMessage, InMessage.WParam, InMessage.LParam);
		}
		}
	}

	void Application::PumpMessage()
	{
		MSG Msg;
		if (PeekMessage(&Msg, NULL, NULL, NULL, PM_REMOVE) > 0)
		{
			TranslateMessage(&Msg);
			DispatchMessage(&Msg);
		}
		if (Msg.message == WM_CLOSE)
		{
			std::cin.get();
		}
	}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
	{
		SMessage Msg;
		Msg.hWnd = hwnd;
		Msg.UMessage = umessage;
		Msg.WParam = wparam;
		Msg.LParam = lparam;

		return (LRESULT)sApplicationHandler->ProcessMessages(std::move(Msg));
	}
} // WindmillEngine