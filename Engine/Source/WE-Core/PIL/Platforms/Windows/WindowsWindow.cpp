#include "PIL/Application/Window.h"

namespace WindmillEngine
{
	bool Window::Initialize()
	{
		if (!mbCanInitialize)
		{
			WE_ERROR("Can't initialize Window, see if the Flags are setted!!!");
			return false;
		}

		DWORD ExStyle = WS_EX_APPWINDOW;
		DWORD Style = 0;
		int32 WindowX = m_X;
		int32 WindowY = m_Y;
		int32 WindowWidth = m_Width;
		int32 WindowHeight = m_Height;

		HINSTANCE hInstance = GetModuleHandle(0);

		if (m_Flags.bIsBorderless)
		{
			ExStyle |= WS_EX_WINDOWEDGE;
			Style = WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
		}
		else
		{
			Style = WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION;
			if (m_Flags.bCanMaximize)
			{
				Style |= WS_MAXIMIZEBOX;
			}
			if (m_Flags.bCanMinimize)
			{
				Style |= WS_MINIMIZEBOX;
			}
			if (m_Flags.bHasSizingBorder)
			{
				Style |= WS_THICKFRAME;
			}
			else
			{
				Style |= WS_BORDER;
			}

			RECT BorderRect = { 0, 0, 0, 0 };
			::AdjustWindowRectEx(&BorderRect, Style, false, ExStyle);

			WindowX += BorderRect.left;
			WindowY += BorderRect.top;

			WindowWidth += BorderRect.right - BorderRect.left;
			WindowHeight += BorderRect.bottom - BorderRect.top;
		}

		m_Handler = CreateWindowEx(
			ExStyle,
			TEXT("WindmillWindow"),
			m_Title.c_str(),
			Style,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			WindowWidth,
			WindowHeight,
			NULL,
			NULL,
			hInstance,
			NULL
		);

		if ((HWND)m_Handler == NULL)
		{
			const uint32 Error = GetLastError();

			WE_ERROR("Window Creation Failed (%d).", Error);

			return false;
		}

		::UpdateWindow((HWND)m_Handler);

		return true;
	}

	void Window::Move(uint32 NewX, uint32 NewY)
	{
		if (!m_Flags.bIsBorderless)
		{
			const LONG WindowStyle = ::GetWindowLong((HWND)m_Handler, GWL_STYLE);
			const LONG WindowExStyle = ::GetWindowLong((HWND)m_Handler, GWL_EXSTYLE);

			RECT BorderRect = { 0, 0, 0, 0 };
			::AdjustWindowRectEx(&BorderRect, WindowStyle, false, WindowExStyle);

			NewX += BorderRect.left;
			NewY += BorderRect.top;
		}

		::SetWindowPos((HWND)m_Handler, nullptr, NewX, NewY, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
	}

#pragma warning(push)
#pragma warning(disable:4100)
	void Window::Resize(uint32 NewWidth, uint32 NewHeight)
	{
		WE_WARN("Not implemented. %s : %d", __FILE__, __LINE__);
	}
#pragma warning(pop)

	void Window::Maximize()
	{
		if (m_Flags.bCanMaximize)
			::ShowWindow((HWND)m_Handler, SW_MAXIMIZE);
	}

	void Window::Minimize()
	{
		if (m_Flags.bCanMinimize)
			::ShowWindow((HWND)m_Handler, SW_MINIMIZE);
	}

	void Window::Show()
	{
		::ShowWindow((HWND)m_Handler, SW_SHOW);
	}

	void Window::Hide()
	{
		::ShowWindow((HWND)m_Handler, SW_HIDE);
	}

	void Window::Close()
	{
		::CloseWindow((HWND)m_Handler);		
	}

	void Window::Destroy()
	{
		::DestroyWindow((HWND)m_Handler);
	}
} // namespace WindmillEngine