#include "PIL/Utils/GUID.h"
#include <rpc.h>

namespace WindmillEngine
{
	SUID::SUID(void) noexcept
	{
		GUID g = GUID();
		HRESULT Res = CoCreateGuid(&g);
		Data1 = 0;
		Data2 = 0;
		Data3 = 0;
		ZeroMemory(Data4, sizeof(Data4));
		if (Res == S_OK)
		{
			Data1 = g.Data1;
			Data2 = g.Data2;
			Data3 = g.Data3;
			memcpy(&Data4, &g.Data4, sizeof(Data4));
		}
	}
}