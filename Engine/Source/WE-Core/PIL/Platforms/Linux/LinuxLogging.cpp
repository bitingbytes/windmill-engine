#include "PIL/Logging.h"

namespace WindmillEngine
{
	void Logger::PlatformLogMessage(ELogLevel InLevel)
	{
		switch (InLevel)
		{
		case LL_Fatal:
            printf("%s", "\e[01;31m");    
			break;
		case LL_Error:
            printf("%s", "\e[05;31m");
			break;
		case LL_Warning:
            printf("%s", "\e[05;33m");
			break;
		case LL_Info:
            printf("%s", "\e[05;32m");        
			break;
        case LL_Verbose:
            break;
		}
	}

	void Logger::PlatformClearLogMessage()
	{
	    printf("%s", "\e[22;37m");	
	}
} // namespace WindmillEngine