#include "PIL/Application/Window.h"

namespace WindmillEngine
{
	bool Window::Initialize()
	{
		return true;
	}

	void Window::Move(uint32 NewX, uint32 NewY)
	{
		
	}

	void Window::Resize(uint32 NewWidth, uint32 NewHeight)
	{
		WE_WARN("Not implemented. %s : %d", __FILE__, __LINE__);
	}

	void Window::Maximize()
	{

	}

	void Window::Minimize()
	{

	}

	void Window::Show()
	{
	}

	void Window::Hide()
	{
	}

	void Window::Close()
	{
	}

	void Window::Destroy()
	{
	}
} // namespace WindmillEngine