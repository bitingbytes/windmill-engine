// <Eremita Copyrights>

#pragma once

#include <signal.h>

#if WE_DEBUG
#define forceinline inline
#define DEBUG_BREAK() raise(SIGTRAP)
#else
#define forceinline inline
#define DEBUG_BREAK() 
#endif

#if defined(WE_EXPORT_DLL)
#define DLLEXPORT __attribute__((visibility("default")))
#else
#define DLLIMPORT 
#endif

#define ALIGN(n) __attribute__((aligned(n)))

#ifndef TEXT
    #if _UNICODE
       #define TEXT(x) L##x
    #else
       #define TEXT(x) x
    #endif
#endif