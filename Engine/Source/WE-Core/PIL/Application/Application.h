// <Eremita Copyrights>

#pragma once

#include "PIL/Containers/String.h"
#include "Window.h"
#include "Time.h"
#include "PIL/Threads/ThreadPool.h"

namespace WindmillEngine
{
	struct SMessage;

	enum class EApplicationState
	{
		AS_None,
		AS_Running,
		AS_Exiting
	};

	class Application
	{
	public:
		Application(const SString& InName, uint32 InWidth = 800, uint32 InHeight = 600, const SWindowFlags& InFlags = SWindowFlags(), uint32 InX = 0, uint32 InY = 0);
		virtual ~Application();

		void Initialize();
		void Run();

		void Start();
		virtual void OnStart();

		// Game loop
		void Update();
		virtual void OnUpdate(const Time& InTime);

		void Render();
		virtual void OnRender();

		void Exit();
		virtual void OnExit();

		void* ProcessMessages(SMessage&& InMessage);

		forceinline void SetApplicationState(EApplicationState NewState)
		{
			m_State = NewState;
		}

		/*forceinline bool IsHereGameThread() const
		{
			return std::this_thread::get_id() == m_GameLoopId;
		}

		forceinline bool IsHereRenderThread() const
		{
			return std::this_thread::get_id() == m_RenderLoopId;
		}
		*/
	protected:
	private:
		std::shared_ptr<Window> m_Window;

		EApplicationState m_State;

		Time m_Time;

		void PumpMessage();

		// std::thread Threads[7];
	};

	static Application* sApplicationHandler = nullptr;
} // WindmillEngine