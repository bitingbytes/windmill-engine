#include "WECorePCH.h"
#include "Window.h"

namespace WindmillEngine
{
	Window::Window() noexcept
		: m_Width(800)
		, m_Height(600)
		, m_X(0)
		, m_Y(0)
		, mbCanInitialize(0)
	{
	}

	Window::~Window()
	{
		Destroy();
	}

	bool Window::Create(const SString& InTitle, uint32 InWidth, uint32 InHeight, uint32 InX, uint32 InY, const SWindowFlags & InFlags)
	{
		m_Title = InTitle;
		m_Width = InWidth;
		m_Height = InHeight;
		m_X = InX;
		m_Y = InY;
		m_Flags = InFlags;

		ValidateFlags(m_Flags);

		return mbCanInitialize;
	}

	void Window::Reshape(uint32 NewX, uint32 NewY, uint32 NewWidth, uint32 NewHeight)
	{
		Move(NewX, NewY);
		Resize(NewWidth, NewHeight);
	}

	void Window::ValidateFlags(SWindowFlags& InFlags)
	{
		InFlags.bHasSizingBorder = !InFlags.bIsBorderless;

		mbCanInitialize = true;
	}
}// namespace WindmillEngine