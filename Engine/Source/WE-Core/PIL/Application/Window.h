// <Eremita Copyrights>

#pragma once

#include "PIL/Containers/String.h"
#include "PIL/Logging.h"

namespace WindmillEngine
{
	struct SWindowFlags
	{
		uint8 bIsWindowned : 1;
		uint8 bIsBorderless : 1;
		uint8 bCanMaximize : 1;
		uint8 bCanMinimize : 1;
		uint8 bHasSizingBorder : 1;

		SWindowFlags() noexcept
		{
			bIsWindowned = 1;
			bIsBorderless = 0;
			bCanMaximize = 1;
			bCanMinimize = 1;
			bHasSizingBorder = 1;
		}
	};

	class Window
	{
	public:
		Window() noexcept;
		~Window();

		bool Create(const SString& InTitle, uint32 InWidth, uint32 InHeight, uint32 InX, uint32 InY, const SWindowFlags & InFlags = SWindowFlags());

		bool Initialize();

		void Move(uint32 NewX, uint32 NewY);

		void Resize(uint32 NewWidth, uint32 NewHeight);

		void Reshape(uint32 NewX, uint32 NewY, uint32 NewWidth, uint32 NewHeight);

		void Maximize();

		void Minimize();

		void Show();

		void Hide();

		void Close();

		void Destroy();

		forceinline void* GetHandler() const
		{
			return m_Handler;
		}

		forceinline bool IsBoardless() const
		{
			return m_Flags.bIsBorderless;
		}

		forceinline bool IsWindowned() const
		{
			return m_Flags.bIsWindowned;
		}

		forceinline bool IsFullscreen() const
		{
			return !IsWindowned();
		}

		forceinline uint32 GetWidth() const
		{
			return m_Width;
		}

		forceinline uint32 GetHeight() const
		{
			return m_Height;
		}

		forceinline SWindowFlags GetWindowFlags() const
		{
			return m_Flags;
		}

		forceinline void SetWidth(uint32 NewWidth)
		{
			m_Width = NewWidth;
		}

		forceinline void SetHeight(uint32 NewHeight)
		{
			m_Height = NewHeight;
		}

		forceinline void SetTitle(SString&& NewTitle)
		{
			m_Title = std::move(NewTitle);
		}

		forceinline void SetTitle(const SString& NewTitle)
		{
			m_Title = NewTitle;
		}

		forceinline void SetWindowFlags(const SWindowFlags& NewWindowFlags)
		{
			m_Flags = NewWindowFlags;
		}

	protected:
		/// <sumary>
		///
		/// </sumary>
		/// <param name="InFlags"></param>
		void ValidateFlags(SWindowFlags& InFlags);

	private:
		uint32 m_Width;
		uint32 m_Height;
		uint32 m_X;
		uint32 m_Y;

		SString m_Title;

		SWindowFlags m_Flags;

		uint8 mbCanInitialize : 1;

		void* m_Handler;

		static const char* AppWindowClass[];
	};
}// namespace WindmillEngine