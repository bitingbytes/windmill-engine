#include "Application.h"

namespace WindmillEngine
{
	Application::Application(const SString & InName, uint32 InWidth, uint32 InHeight, const SWindowFlags & InFlags, uint32 InX, uint32 InY)
	{
		m_Window = std::make_shared<Window>();
		m_Window->Create(InName, InWidth, InHeight, InX, InY, InFlags);
		m_State = EApplicationState::AS_None;
	}

	Application::~Application()
	{
	}

	void Application::Start()
	{
		if (!m_Window->Initialize())
		{
			WE_ERROR("Could not Initialize Window!!!");
			return;
		}
		m_Window->Show();
		m_State = EApplicationState::AS_Running;

		OnStart();

		/*auto GameTask = DefaultThreadPool::submitJob([](Application* App) {
			App->Update();
		}, this);

		auto RenderTask = DefaultThreadPool::submitJob([](Application* App) {
			App->Render();
		}, this);*/

		Run();
	}

	void Application::OnStart()
	{
	}

	void Application::Update()
	{
		//while (m_State == EApplicationState::AS_Running)
		//{
			OnUpdate(m_Time);
		//}
	}

	void Application::OnUpdate(const Time& InTime)
	{
	}

	void Application::Render()
	{
		//while (m_State == EApplicationState::AS_Running)
		//{
			OnRender();
		//}
	}

	void Application::OnRender()
	{
	}

	void Application::Run()
	{
		while (m_State == EApplicationState::AS_Running)
		{
			PumpMessage();

			{
				Update();
			}
			{
				Render();
			}
		}
		Exit();
	}

	void Application::Exit()
	{
		m_Window->Close();
		OnExit();
	}

	void Application::OnExit()
	{		
	}
} // WindmillEngine