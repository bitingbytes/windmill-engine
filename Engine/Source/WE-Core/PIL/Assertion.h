// <Eremita Copyrights>

#pragma once

#include "PIL/PlatformDetection.h"

#ifdef WE_DEBUG
#define WE_ASSERT(x) \
			if (!(x)) {\
				std::cout << "*************************" <<std::endl;\
				std::cout << "*   ASSERTION FAILED!   *" <<std::endl;\
				std::cout << "*************************" <<std::endl;\
				std::cout << __FILE__<< ": " << __LINE__ <<std::endl;\
				std::cout << "Condition: "<< #x <<std::endl;\
				DEBUG_BREAK(); \
			}
#else
#define WE_ASSERT(x) 
#endif
