// "Eremita Copyrights"

#pragma once

#include "AtomicTypes.h"

#if !defined(WE_PLATFORM_WINDOWS)
#define WE_PLATFORM_WINDOWS 0
#endif
#if !defined(WE_PLATFORM_LINUX)
#define WE_PLATFORM_LINUX 0
#endif
#if !defined(WE_PLATFORM_MACOS)
#define WE_PLATFORM_MACOS 0
#endif
#if !defined(WE_PLATFORM_PS4)
#define WE_PLATFORM_PS4 0
#endif
#if !defined(WE_PLATFORM_XBOXONE)
#define WE_PLATFORM_XBOXONE 0
#endif
#if !defined(WE_PLATFORM_SWITCH)
#define WE_PLATFORM_SWITCH 0
#endif
#if !defined(WE_PLATFORM_EMBEDDED)
#define WE_PLATFORM_EMBEDDED 0
#endif
#if !defined(WE_PLATFORM_ANDROID)
#define WE_PLATFORM_ANDROID 0
#endif
#if !defined(WE_PLATFORM_IOS)
#define WE_PLATFORM_IOS 0
#endif

// Platform Commons
#if WE_PLATFORM_WINDOWS
#include "Platforms/Windows/WindowsCommon.h"
#elif WE_PLATFORM_LINUX
#include  "Platforms/Linux/LinuxCommon.h"
#elif WE_PLATFORM_MACOS
#include  "Platforms/MacOS/MacCommon.h"
#elif WE_PLATFORM_PS4
#include  "Platforms/Ps4/Ps4Common.h"
#elif WE_PLATFORM_XBOXONE
#include  "Platforms/Xbox/XboxCommon.h"
#elif WE_PLATFORM_SWITCH
#include  "Platforms/Switch/SwitchCommon.h"
#elif WE_PLATFORM_EMBEDDED
#include  "Platforms/Embedded/EmbeddedCommon.h"
#elif WE_PLATFORM_ANDROID
#include  "Platforms/Android/AndroidCommon.h"
#elif WE_PLATFORM_IOS
#include  "Platforms/IOS/IOSCommon.h"
#else
#error Invalid Compiler
#endif