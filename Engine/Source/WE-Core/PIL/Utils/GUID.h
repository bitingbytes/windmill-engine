#pragma once

#include "WECore.h"

namespace WindmillEngine
{
	class SUID
	{
	public:
		SUID(void) noexcept;

		uint32 Data1;
		uint16  Data2;
		uint16  Data3;
		uint8  Data4[8];
	};
}